/** 
 * Title: Festival navigation
* @autor : kyjhg1@naver.com
* Last Update : 2013/05/26
* version 4.0
*/ 
package com.example.ftnavi;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.maps.MapView;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;

/*
 activity_map 동작을 위한 클래스
* */
public class MapActivity extends FragmentActivity {

	private GoogleMap map; 
	static double lat,lng; //위도와 경도를 저장하는 변수
	private LatLng position; //위도와 경도로 위치를 표현하기위해 사용
	MapView mapview;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		map=((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
		position=new LatLng(lat,lng);
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 15));
		map.addMarker(new MarkerOptions().position(new LatLng(lat,lng)).title("Marker Test").icon(BitmapDescriptorFactory.fromResource(R.drawable.where)));
	}
//	public void CreateMarker(Double geolat,Double geolng)
	//{
	//mapview.addMarker(new MarkerOptions().position(new LatLng(geolat,geolng)).title("Marker Test").icon(BitmapDescriptorFactory.fromResource(R.drawable.where)));  
	//}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
