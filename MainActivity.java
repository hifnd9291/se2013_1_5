/** 
 * Title: Festival navigation
* @autor : kyjhg1@naver.com
* Last Update : 2013/05/26
* version 4.0
*/ 
package com.example.ftnavi;

import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.Button;
/*
@param :activity_main을 위한 클래스
        처음 로딩화면다음에 나타나는 화면
* */
import android.widget.Toast;

public class MainActivity extends Activity {
	/*
	@param :savedInstanceState
	       activity_main 동작을 위한 메소드
	* */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main); //activity_main의 레이아웃을 보여준다.
         
        //로딩화면띄우기 
        startActivity(new Intent(this, SubActivity.class));
    //Button
    Button btnCall = (Button)findViewById(R.id.button1); //정보보기를 위한 버튼
    Button btnCal2 = (Button)findViewById(R.id.button2); //나의 축제를 보기위한 버튼
    Button btnCal3 = (Button)findViewById(R.id.button3); //앱 종료를 위한 버튼
    
    /*
     *@param :버튼을 클릭하면 onClick메소드로 인해서 SubActivity로 이동한다. 
     */
    btnCall.setOnClickListener(new Button.OnClickListener()  {
    	public void onClick(View v) {
    		//버튼클릭시 진동을 울리게 한다.
    		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
        	vib.vibrate(80);
    		Intent intent = new Intent(MainActivity.this, Sub2Activity.class);
    		startActivity(intent);
    	}
    	
        });
    
    /*
     *@param :버튼을 클릭하면 onClick메소드로 인해서 트위터로 연결된다.
     */
    btnCal2.setOnClickListener(new Button.OnClickListener()  {
    	public void onClick(View v) {
    		//버튼클릭시 진동을 울리게 한다.
    		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
        	vib.vibrate(80);
        	MutiTwitter twitter=(MutiTwitter)new MutiTwitter().execute((Void)null);
    	}
    	
        });
    /*
     *@param :버튼을 클릭하면 onClick메소드로 인해서 대화상자가 뜬다.
     *        PositiveButton:종료버튼
     *        NegativeButton: 대화상자만 종료
     */
    btnCal3.setOnClickListener(new Button.OnClickListener()
    {
         public void onClick(View v) 
         {		
        	//버튼클릭시 진동을 울리게 한다.
     		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
         	vib.vibrate(80);
    	   AlertDialog.Builder bld = new AlertDialog.Builder(MainActivity.this);
    		bld.setTitle("정말 그만할거야ㅠㅠ?");
    		bld.setIcon(R.drawable.hello);
    		bld.setPositiveButton("그만할거야!", new DialogInterface.OnClickListener() 
    		{
    	    	public void onClick(DialogInterface dialog, int which)
    	    	{
    	    		//버튼클릭시 진동을 울리게 한다.
    	    		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
    	        	vib.vibrate(80);
    				//moveTaskToBack(true);
    				finish();	
    	         }
         });
    		bld.setNegativeButton("계속할거야", null).show();
         }
    });
    }
    class MutiTwitter extends AsyncTask<Void, Void, Void> 
    {
    	@Override
    	protected Void doInBackground(Void...arg0)
    	{
    	Intent tintent=new Intent(MainActivity.this,TwitterMain.class);
    	startActivity(tintent);
    	return null;
        }
    
    @Override
    protected void onProgressUpdate(Void...values)
    {
    	Intent tintent=new Intent(MainActivity.this,TwitterMain.class);
    	startActivity(tintent);
    	super.onProgressUpdate(values);
    }
   
   
    }
    
   protected void onActivityResult(int requestCode, int resultCode, Intent resultIntent)
    {
    	super.onActivityResult(requestCode, resultCode, resultIntent);
    	
    	if (requestCode == TwitFlag.REQ_CODE_TWIT_LOGIN)
    	{
    		if (resultCode == RESULT_OK) 
    		{
    					
    			Toast.makeText(getBaseContext(), "Twitter connection succeeded : " + TwitFlag.TWIT_KEY_TOKEN, Toast.LENGTH_LONG).show();	
    		}
    	}
    }
}
