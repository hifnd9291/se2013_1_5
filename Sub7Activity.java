/** 
 * Title: Festival navigation
* @autor : kyjhg1@naver.com
* Last Update : 2013/05/26
* version 4.0
*/  
package com.example.ftnavi;

import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;
import android.widget.TabHost;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

/*activity_sub7을 위한 클래스(전라남도)* */

public class Sub7Activity extends Activity 
{
	private int springcount=0; 
	private int summercount=0; 
	private int fallcount=0;   
    private int wintercount=0; 

    /** Called when the activity is first created. */
    @Override
    /*
    @param :activity_sub7을 위한 메소드
    * */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_sub7);
         {
        	 
        	  /*봄축제에 관한 버튼 * */
        	 /*:클릭하면 축제의 정보보여주기위한 대화상자가 나타난다 * */
       final Button button1 = (Button)findViewById(R.id.button1);  //Sub3spring1Activity (양구곰취축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub7_spirng1==0)
            button1.setBackgroundResource(R.drawable.where);
           else
        	   button1.setBackgroundResource(R.drawable.changewhere);
        button1.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=35.2790765;
      		     MapActivity.lng=127.307875;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub7Activity.this);
        		bld.setTitle("곡성세계 장미축제");
        	   bld.setMessage("전남 곡성군 섬진강기차마을 장미공원기간2013.05.24~2013.06.02");
        		
        	 bld.setIcon(R.drawable.sub7_spring1);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
                	Intent intent = new Intent(Sub7Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button1.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub7_spirng1=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
        
       final Button button2 = (Button)findViewById(R.id.button2);  //Sub3spring2Activity (백두대간 내면 나물축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub7_spirng2==0)
            button2.setBackgroundResource(R.drawable.where);
           else
        	   button2.setBackgroundResource(R.drawable.changewhere);
        button2.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=35.3267638;
      		     MapActivity.lng=126.9863217;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub7Activity.this);
        		bld.setTitle("담양대나무축제 ");
        	   bld.setMessage("호남의 젖줄인 영산강의 시원, 맑은 물을 머금고 자라는 대나무! 들리시나요? 그 대나무 숲에서 불어오는 상쾌한 바람소리! 사각사각 들리는 댓잎소리! 감미로운 대숲향기가 여러분을 기다리고 있습니다. 장소전남 담양군 죽녹원, 담양관방제림 일원기간2013.05.03~2013.05.08");
        		
        	 bld.setIcon(R.drawable.sub7_spring2);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub7Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
     	 {
     		 public void onClick(DialogInterface dialog, int which)
   	    	{
   	    		//버튼클릭시 진동을 울리게 한다.
          		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
              	vib.vibrate(80);
              	button2.setBackgroundResource(R.drawable.changewhere);
              	flag.sub7_spirng2=1;
   	         }
        });
        	 bld.show();
             }
        });
        
        
       final Button button3 = (Button)findViewById(R.id.button3);  //Sub3spring3Activity (커피나무축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub7_spirng3==0)
            button3.setBackgroundResource(R.drawable.where);
           else
        	   button3.setBackgroundResource(R.drawable.changewhere);
        button3.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=34.7450914;
      		     MapActivity.lng=127.0814453;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub7Activity.this);
        		bld.setTitle("보성다향제");
        	   bld.setMessage("우리 보성은 전국 최대의 차주산지로서 차산업과 차문화 보급발전을 위해 해마다 보성녹차대축제를 개최하고 있습니다. 금년에는 5월 14일부터 19일까지 6일간 한국차소리문화공원에서 진행되며 개막식은 5월 14일 오후 5시에 티월드챔피언십(한국차스승헌다례, 찻자리경연, 황실혼례복식패션쇼)과 함께 진행됩니다. 장소전남 보성군 한국차소리문화공원 등기간2013.05.14~2013.05.19");
        		
        	 bld.setIcon(R.drawable.sub7_spring3);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub7Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button3.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub7_spirng3=1;
      	         }
           });
        	 bld.show();
             }
        });
       final Button button4 = (Button)findViewById(R.id.button4);  //Sub3spring3Activity (커피나무축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub7_spirng4==0)
            button4.setBackgroundResource(R.drawable.where);
           else
        	   button4.setBackgroundResource(R.drawable.changewhere);
        button4.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {	
            	 MapActivity.lat=34.4905206;
      		     MapActivity.lng=126.3430118;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub7Activity.this);
        		bld.setTitle("진도 신비의 바닷길 축제");
        	   bld.setMessage("세계적으로 널리 알려진 진도 신비의 바닷길은 고군면 회동리(古郡面 回洞里)와 의신면 모도리(義新面 茅島里) 사이 약 2.8km가 조수간만의 차이로 수심이 낮아질 때 바닷길이 드러나는 현상이지만 40여m의 폭으로 똑같은 너비의 길이 바닷속에 만들어진다는데 신비로움이 있다. 장소전남 진도군 회동리~의신면 모도리기간2013.04.25~2013.04.28");
        		
        	 bld.setIcon(R.drawable.sub7_spring4);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub7Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button4.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub7_spirng4=1;
      	         }
           });
        	 bld.show();
             }
        });
       final Button button5 = (Button)findViewById(R.id.button5);  //Sub3spring3Activity (커피나무축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub7_spirng5==0)
            button5.setBackgroundResource(R.drawable.where);
           else
        	   button5.setBackgroundResource(R.drawable.changewhere);
        button5.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=35.2639085;
      		     MapActivity.lng=127.5460357;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub7Activity.this);
        		bld.setTitle("구례 산수유꽃축제");
        	   bld.setMessage("제일 먼저 봄소식을 전하는 산수유꽃, 노오란 산수유꽃은 봄입니다. 봄은 희망입니다. 희망으로 가득찬 이 곳에서 사랑하는 가족과 함께 봄을 느껴보시길 바랍니다. 장소전남 구례군 지리산온천관광단지기간2013.03.29~2013.03.31");
        		
        	 bld.setIcon(R.drawable.sub7_spring5);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub7Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button5.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub7_spirng5=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
       final Button button6 = (Button)findViewById(R.id.button6);  //Sub3spring3Activity (커피나무축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub7_spirng6==0)
            button6.setBackgroundResource(R.drawable.where);
           else
        	   button6.setBackgroundResource(R.drawable.changewhere);
        button6.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=34.6846473;
      		     MapActivity.lng=127.0361717;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub7Activity.this);
        		bld.setTitle("보성 일림산철쭉제");
        	   bld.setMessage("2000년부터 개발된 일림산철쭉은 100ha 이상으로 전국최대의 철쭉군락지를 자랑하며, 제암산과 사자산으로 연결되는 철쭉군락지의 길이는 12.4㎞에 달하여 가히 세계적이라 추켜 세울만 하고 남도 전역을 빨갛게 물들이고 봉우리마다 분홍빛으로 얼룩진 모습은 어머니의 가슴보다도 더 아름다워 보는 이의 마음을 빼앗아가 두 번 이상 보아야 빼앗긴 마음을 되찾을 수 있다고 한다. 장소전남 보성군 일림산 용추계곡 주차장기간2013.05.04~2013.05.06");
        		
        	 bld.setIcon(R.drawable.sub7_spring6);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub7Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button6.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub7_spirng6=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
        /*여름축제에 관한 버튼 * */	 
      final Button button7 = (Button)findViewById(R.id.button7);  //Sub3summer1Activity (의암제)정보로 이동하기 위한 버튼
        summercount++;
        if(flag.sub7_summer1==0)
            button7.setBackgroundResource(R.drawable.where);
           else
        	   button7.setBackgroundResource(R.drawable.changewhere);
        //button4.setPadding(10, 10, 10, 10);
        button7.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=34.792;
      		     MapActivity.lng=127.5778995;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub7Activity.this);
        		bld.setTitle("여자만 갯벌 노을 축제");
        	   bld.setMessage("갯벌노을축제는 전라남도 여수시 소라면 사곡리 바닷가 일원에서 갯벌체험과 노을을 주제로 2008년부터 개최되고 있는 아름다운 축제입니다. 여자만은 바다 가운데 여자도(汝自島)라는 섬이 있어 붙은 이름으로 여수, 순천, 벌교, 보성을 포함한 큰 바다입니다. 장소전남 여수시 장척마을 일원기간2013.06.21~2013.06.23");
        		
        	 bld.setIcon(R.drawable.sub7_summer1);
        	
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub7Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button7.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub7_summer1=1;
      	         }
           });
        	 bld.show();
             }
        });
        
       
        
       final Button button8 = (Button)findViewById(R.id.button8);  //Sub3summer2Activity (원주 장미축제)정보로 이동하기 위한 버튼
       // button5.setPadding(20, 20, 20, 20);
        summercount++;
        if(flag.sub7_summer2==0)
            button8.setBackgroundResource(R.drawable.where);
           else
        	   button8.setBackgroundResource(R.drawable.changewhere);
        button8.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=34.9499498;
      		     MapActivity.lng=127.4947343;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub7Activity.this);
        		bld.setTitle("순천하늘빛축제");
        	   bld.setMessage("전남 순천시 동천장대공원 일원기간2013.06.08~2013.10.27");
        		
        	 bld.setIcon(R.drawable.sub7_summer2);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub7Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button8.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub7_summer2=1;
      	         }
           });
        	 bld.show();
             }
        });
        
     
       final Button button9 = (Button)findViewById(R.id.button9);  //Sub3summer3Activity (강릉단오제)정보로 이동하기 위한 버튼
        summercount++;
        if(flag.sub7_summer3==0)
            button9.setBackgroundResource(R.drawable.where);
           else
        	   button9.setBackgroundResource(R.drawable.changewhere);
        button9.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=34.6775766;
      		     MapActivity.lng=126.8502363;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub7Activity.this);
        		bld.setTitle("정남진 장흥 물축제 ");
        	   bld.setMessage("정남진 장흥물축제는 우리 군이 자랑하는 탐진강 하천, 장흥댐 호수, 특량만 해수 등 청정 수자원을 기반으로 하는 테마축제로 푸른 자연 못지않게 깨끗하고 바른 지역의 이미지를 안팎에 알림으로써 관광객 증가 → 유기농 농산물 판매촉진 및 문화유산 등 관광자원 홍보 → 관광수입 증가를 통한 지역경제 활성화에 큰 목적을 두고 시작하였습니다. 장소전남 장흥군 탐진강, 편백숲 우드랜드 일원기간2013.07.26~2013.08.01");
        		
        	 bld.setIcon(R.drawable.sub7_summer3);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub7Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button9.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub7_summer3=1;
      	         }
           });
        	 bld.show();
             }
        });
        
      final Button button10 = (Button)findViewById(R.id.button10);  //Sub3summer3Activity (강릉단오제)정보로 이동하기 위한 버튼
        summercount++;
        if(flag.sub7_summer4==0)
            button10.setBackgroundResource(R.drawable.where);
           else
        	   button10.setBackgroundResource(R.drawable.changewhere);
        button10.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=34.990489;
      		     MapActivity.lng=126.4817121;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub7Activity.this);
        		bld.setTitle("무안연꽃축제");
        	   bld.setMessage("동양최대 10만평을 가득 채운 초록빛 연잎 사이로 고결함을 드러내듯 올곧이 하얀꽃망울을 띄우는 백련! 자연의 한가운데서 다채로운 행사와 함께 여름의 낭만을 담아가시기 바랍니다. 장소전남 무안군 회산 백련지 일원기간2013.07.24~2013.07.28");
        		
        	 bld.setIcon(R.drawable.sub7_summer4);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub7Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button10.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub7_summer4=1;
      	         }
           });
        	 bld.show();
             }
        });
       
       
        /*가을축제에 관한 버튼 * */
        final Button button11 = (Button)findViewById(R.id.button11);   //Sub3fall1Activity (정선아리랑제 축제)정보로 이동하기 위한 버튼
        fallcount++;
        if(flag.sub7_fall1==0)
            button11.setBackgroundResource(R.drawable.where);
           else
        	   button11.setBackgroundResource(R.drawable.changewhere);
        button11.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=34.4786676;
      		     MapActivity.lng=126.826213; 
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub7Activity.this);
        		bld.setTitle("마량미항축제");
        	   bld.setMessage("청자골 해상관문 마량 미항은 전국 최초로 어촌어항 복합공간으로 살아있는 청정해와 천연기념물 마량까막섬 상록수림과 마량-고금 연육교의 아름다운 미항을 주제로 하여 마량미항축제를 통해 지역주민의 역량을 하나로 결집하여, 관광객 유치 및 지역경제 활성화에 기여하고자 개최함. 장소전남 강진군 마량면 일원기간2013.09.13~2013.09.15");
        		
        	 bld.setIcon(R.drawable.sub7_fall1);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub7Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button11.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub7_fall1=1;
      	         }
           });
        	 bld.show();
             }
        });
      
        
      final  Button button12 = (Button)findViewById(R.id.button12);   //Sub3fall2Activity (양양송이축제)정보로 이동하기 위한 버튼
        fallcount++; 
        if(flag.sub7_fall2==0)
            button12.setBackgroundResource(R.drawable.where);
           else
        	   button12.setBackgroundResource(R.drawable.changewhere);
        button12.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=34.9069578;
      		     MapActivity.lng=127.5012249; 
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub7Activity.this);
        		bld.setTitle("순천만국제정원박람회 ");
        	   bld.setMessage("지구상에서 가장 온전하게 보존된 세계5대연안습지 순천만과 함께 세계 최고의 생태정원을 보유하게 되어 우리시는 대한민국 대표적인 친환경 생태도시, 녹색성장 장소전남 순천시 순천만 일원기간2013.04.20~2013.10.20");
        		
        	 bld.setIcon(R.drawable.sub7_fall2);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub7Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button12.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub7_fall2=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        /*겨울축제에 관한 버튼 * */
        
       final Button button13 = (Button)findViewById(R.id.button13);   //Sub3fall2Activity (양양송이축제)정보로 이동하기 위한 버튼
        wintercount++; 
        if(flag.sub7_winter1==0)
            button13.setBackgroundResource(R.drawable.where);
           else
        	   button13.setBackgroundResource(R.drawable.changewhere);
        button13.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=34.5945478;
      		     MapActivity.lng=127.8024658;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub7Activity.this);
        		bld.setTitle("여수향일암일출제");
        	   bld.setMessage("국내 최고의 해돋이 명소인 향일암은 우리나라 4대 관음 기도처 중 하나로 이 곳에서 기도를 드리면 소원이 이뤄진다고 합니다. 기암괴석과 탁 트인 수평선을 마주하며 새해 소원성취의 기운을 느껴보시기 바랍니다. 장소전남 여수시 임포마을기간2013.12.31~2014.01.01");
        		
        	 bld.setIcon(R.drawable.sub7_winter1);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub7Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button13.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub7_winter1=1;
      	         }
           });
        	 bld.show();
             }
        });
        
      
        /*봄, 여름, 가을, 겨울 구분하기 위한 탭 생성* */
        TabHost tabHost = (TabHost)findViewById(R.id.tabHost);
        tabHost.setup();
     
        TabHost.TabSpec spec = tabHost.newTabSpec("tag1"); //봄을 나타내는 tab1
       spec.setContent(R.id.tab1);
       spec.setIndicator("", this.getResources().getDrawable(R.drawable.tab1));   	      	
       tabHost.addTab(spec);

        
        spec = tabHost.newTabSpec("tag2");  //여름을 나타내는 tab2
        spec.setContent(R.id.tab2);
        spec.setIndicator("", this.getResources().getDrawable(R.drawable.tab2));   	      	
        tabHost.addTab(spec);
        
        
        spec = tabHost.newTabSpec("tag3"); //가을을 나타내는 tab3
        spec.setContent(R.id.tab3);
        spec.setIndicator("", this.getResources().getDrawable(R.drawable.tab3));   	      	 
        tabHost.addTab(spec);
        
        spec = tabHost.newTabSpec("tag5"); //겨울을 나타내는 tab4
        spec.setContent(R.id.tab5);
        spec.setIndicator("", this.getResources().getDrawable(R.drawable.tab4));   	      	
        tabHost.addTab(spec);
        
     
        tabHost.setCurrentTab(0);        
         }
       }
}
