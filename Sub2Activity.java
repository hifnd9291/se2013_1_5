/** 
 * Title: Festival navigation
* @autor : kyjhg1@naver.com
* Last Update : 2013/05/26
* version 4.0
*/ 
package com.example.ftnavi;

import android.os.Bundle;
import android.os.Vibrator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

/*activity_sub2을 위한 클래스
 전체지도 레이아웃을 보여주기 위한 클래스* */
public class Sub2Activity extends Activity {
	  /*
    @param :savedInstanceState
           activity_sub2 동작을 위한 메소드
    * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub2);
        
        Button button1 = (Button)findViewById(R.id.button1); //강원도버튼
        Button button2 = (Button)findViewById(R.id.button2); //수도권버튼
        Button button3 = (Button)findViewById(R.id.button3); //충청남도버튼
        Button button4 = (Button)findViewById(R.id.button4); //충청북도버튼
        Button button5 = (Button)findViewById(R.id.button5); //경상북도버튼
        Button button6 = (Button)findViewById(R.id.button6); //전라북도버튼
        Button button7 = (Button)findViewById(R.id.button7); //경상남도버튼
        Button button8 = (Button)findViewById(R.id.button8); //전라남도버튼
       
        //강원도지도를 보여주는 Sub3Activity로 이동
        button1.setOnClickListener(new Button.OnClickListener()  {
        	public void onClick(View v) {
        		//버튼클릭시 진동을 울리게 한다.
        		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
            	vib.vibrate(80);
        		Intent intent = new Intent(Sub2Activity.this, Sub3Activity.class);
        		startActivity(intent);
        	}
            });
        
        //수도권지도를 보여주는  Sub5Activity로 이동
        button2.setOnClickListener(new Button.OnClickListener()  {
        	public void onClick(View v) {
        		//버튼클릭시 진동을 울리게 한다.
        		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
            	vib.vibrate(80);
        		Intent intent = new Intent(Sub2Activity.this, Sub5Activity.class);
        		startActivity(intent);
        	}
            });
        
        //충청남도지도를 보여주는  Sub4Activity로 이동
        button3.setOnClickListener(new Button.OnClickListener()  {
        	public void onClick(View v) {
        		//버튼클릭시 진동을 울리게 한다.
        		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
            	vib.vibrate(80);
        		Intent intent = new Intent(Sub2Activity.this, Sub4Activity.class);
        		startActivity(intent);
        	}
            });
        
        //충청북도지도를 보여주는  Sub6Activity로 이동
        button4.setOnClickListener(new Button.OnClickListener()  {
        	public void onClick(View v) {
        		//버튼클릭시 진동을 울리게 한다.
        		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
            	vib.vibrate(80);
        		Intent intent = new Intent(Sub2Activity.this, Sub6Activity.class);
        		startActivity(intent);
        	}
            });
        
        //경상북도지도를 보여주는  Sub9Activity로 이동
        button5.setOnClickListener(new Button.OnClickListener()  {
        	public void onClick(View v) {
        		//버튼클릭시 진동을 울리게 한다.
        		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
            	vib.vibrate(80);
        		Intent intent = new Intent(Sub2Activity.this, Sub9Activity.class);
        		startActivity(intent);
        	}
            });
        
        //전라북도지도를 보여주는  Sub8Activity로 이동
        button6.setOnClickListener(new Button.OnClickListener()  {
        	public void onClick(View v) {
        		//버튼클릭시 진동을 울리게 한다.
        		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
            	vib.vibrate(80);
        		Intent intent = new Intent(Sub2Activity.this, Sub8Activity.class);
        		startActivity(intent);
        	}
            });
        
        //경상남도지도를 보여주는  Sub10Activity로 이동
        button7.setOnClickListener(new Button.OnClickListener()  {
        	public void onClick(View v) {
        		//버튼클릭시 진동을 울리게 한다.
        		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
            	vib.vibrate(80);
        		Intent intent = new Intent(Sub2Activity.this, Sub10Activity.class);
        		startActivity(intent);
        	}
            });
        
        //전라남도지도를 보여주는  Sub7Activity로 이동
        button8.setOnClickListener(new Button.OnClickListener()  {
        	public void onClick(View v) {
        		//버튼클릭시 진동을 울리게 한다.
        		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
            	vib.vibrate(80);
        		Intent intent = new Intent(Sub2Activity.this, Sub7Activity.class);
        		startActivity(intent);
        	}
            });  
    }

    /*
    @param :menu
       menu버튼을 위한 메소드
    * */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }   
}
