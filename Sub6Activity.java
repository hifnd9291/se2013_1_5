/** 
 * Title: Festival navigation
* @autor : kyjhg1@naver.com
* Last Update : 2013/05/26
* version 4.0
*/ 
package com.example.ftnavi;

import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;
import android.widget.TabHost;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

/*activity_sub6을 위한 클래스(충청북도)* */
public class Sub6Activity extends Activity 
{
	private int springcount=0; 
	private int summercount=0; 
	private int fallcount=0;   
    private int wintercount=0; 

    /** Called when the activity is first created. */
    @Override
    /*
    @param :activity_sub6을 위한 메소드
    * */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_sub6);
         {
        	 
        	  /*:봄축제에 관한 버튼 * */
        	 /*:클릭하면 축제의 정보보여주기위한 대화상자가 나타난다  * */
       final Button button1 = (Button)findViewById(R.id.button1);  //Sub3spring1Activity (양구곰취축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub6_spirng1==0)
            button1.setBackgroundResource(R.drawable.where);
           else
        	   button1.setBackgroundResource(R.drawable.changewhere);
        button1.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=36.8228524;
      		     MapActivity.lng=127.4844419;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub6Activity.this);
        		bld.setTitle("생거진천 농다리축제");
        	   bld.setMessage("농다리축제는 이같이 소중한 문화유산을 널리 알리고 소중한 문화유산 보존의 중요성을 일깨움은 물론 조상의 슬기를 배워가고자 농다리를 테마로한 이색적 축제이다. 장소충북 진천군 농다리 일원기간2013.05.31~2013.06.02");
        		
        	 bld.setIcon(R.drawable.sub6_spring1);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
                	Intent intent = new Intent(Sub6Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button1.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub6_spirng1=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
        
        final Button button2 = (Button)findViewById(R.id.button2);  //Sub3spring2Activity (백두대간 내면 나물축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub6_spirng2==0)
            button2.setBackgroundResource(R.drawable.where);
           else
        	   button2.setBackgroundResource(R.drawable.changewhere);
        button2.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=36.3076708;
      		     MapActivity.lng=127.5733624;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub6Activity.this);
        		bld.setTitle("옥천 참옻순축제");
        	   bld.setMessage("옻산업특구로 지정받은 옥천군에서 옻나무를 식재한 농업인 및 관련업게 관계자들이 주축이 되어 옥천의 참옻과 옻의 우수성을 알리기위해 매년 개최되는 축제이다. 옻 비누만들기등을 체험할 수 있으며, 일반참가자들이 참여할 수 있는 여러 행사가 마련된다. 해당축제를 통해 일상생활에서의 옻의 다양한 쓰임새를 알 수가 있다. 장소충북 옥천군 향수공원기간2013.05.11~2013.05.12");
        		
        	 bld.setIcon(R.drawable.sub6_spring2);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub6Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button2.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub6_spirng2=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
        final Button button3 = (Button)findViewById(R.id.button3);  //Sub3spring3Activity (커피나무축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub6_spring3==0)
            button3.setBackgroundResource(R.drawable.where);
           else
        	   button3.setBackgroundResource(R.drawable.changewhere);
        button3.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {	
            	 MapActivity.lat=36.1530181;
      		     MapActivity.lng=128.0533452;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub6Activity.this);
        		bld.setTitle("앙성탄산온천휴양축제");
        	   bld.setMessage("전국 유일의 탄산온천수의 영원한 용출을 기원하며 우리지역 천혜의 관광자원과 연계하는 관광축제로 발전시켜 지역발전의 터전구축과 충주 관문으로서의 중추적 역할을 담당하여 21C 한반도의 중심 도시 충주 건설에 기여한다. 수안보온천과 함께 충주를 부각시킬 수 있는 축제로 발전지역 주민과 관광객이 함께하는 축제로 승화시킴으로써 관광객 유치를 통해 지역경제활성화 도모 및 온천개발에 일익을 담당한다. 장소충북 충주시 앙성온천광장 등기간2013.05.31~2013.06.02");
        		
        	 bld.setIcon(R.drawable.sub6_spring3);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub6Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button3.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub6_spring3=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
        
        
        
        /*여름축제에 관한 버튼 * */	 
       final Button button4 = (Button)findViewById(R.id.button4);  //Sub3summer1Activity (의암제)정보로 이동하기 위한 버튼
        summercount++;
        if(flag.sub6_summer1==0)
            button4.setBackgroundResource(R.drawable.where);
           else
        	   button4.setBackgroundResource(R.drawable.changewhere);
        //button4.setPadding(10, 10, 10, 10);
        button4.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {	
            	 MapActivity.lat=36.9674682;
      		     MapActivity.lng=128.426485;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub6Activity.this);
        		bld.setTitle("단양 소백산 철쭉제");
        	   bld.setMessage("매년 5월 말~6월초에 개최되는 소백산 철쭉제는 소백산에 넓게 산재된 철쭉군락을 소재로 개최되는 축제로 소백산 특유의 연분홍빛 철쭉향기에 흠뻑 취해봄으로써 일상에 지친 우리에게 새로운 활력과 에너지를 충전할 수 있는 기회를 제공할 것입니다. 장소충북 단양군 남한강변 및 소백산기간2013.05.29~2013.06.02");
        		
        	 bld.setIcon(R.drawable.sub6_summer1);
        	
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub6Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button4.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub6_summer1=1;
      	         }
           });
        	 bld.show();
             }
        });
        
       
        
       final Button button5 = (Button)findViewById(R.id.button5);  //Sub3summer2Activity (원주 장미축제)정보로 이동하기 위한 버튼
       // button5.setPadding(20, 20, 20, 20);
        summercount++;
        if(flag.sub6_summer2==0)
            button5.setBackgroundResource(R.drawable.where);
           else
        	   button5.setBackgroundResource(R.drawable.changewhere);
        button5.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=36.123617;
      		     MapActivity.lng=127.7223881;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub6Activity.this);
        		bld.setTitle("영동포도축제");
        	   bld.setMessage("이번 축제는 포도따기 체험, 와인만들기, 포도밟기 등 각종체험행사와 다양한 이벤트를 마련하여 지금까지 경험해 보지 못한 새로운 패러다임의 축제로 만나실 수 있습니다. 포도와 와인의 고장 영동으로 여러분을 초대합니다. 장소충북 영동군 영동체육관 일원기간2013.08.30~2013.09.01");
        		
        	 bld.setIcon(R.drawable.sub6_summer2);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub6Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button5.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub6_summer2=1;
      	         }
           });
        	 bld.show();
             }
        });
        
     
       final Button button6 = (Button)findViewById(R.id.button6);  //Sub3summer3Activity (강릉단오제)정보로 이동하기 위한 버튼
        summercount++;
        if(flag.sub6_summer3==0)
            button6.setBackgroundResource(R.drawable.where);
           else
        	   button6.setBackgroundResource(R.drawable.changewhere);
        button6.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=36.9816489;
      		     MapActivity.lng=127.9366799;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub6Activity.this);
        		bld.setTitle("충주 복숭아축제");
        	   bld.setMessage("2009년도 충주재배면적은 1,046ha(전국의 6%)로 전국 자치단체 중 4위를 차지하여 충주복숭아의 우수성을 대내외에 널리 알려 충주는 복숭아의 고장이라는 전국적 이미지 제고, 복숭아재배 농가의 자긍심을 고취시키는 계기 마련. 복숭아 수확의 기쁨을 시민과 함께 하는 한마당 축제로 승화발전. 장소충북 충주시 충주실내체육관기간2013.08.24~2013.08.24");
        		
        	 bld.setIcon(R.drawable.sub6_summer3);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub6Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button6.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub6_summer3=1;
      	         }
           });
        	 bld.show();
             }
        });
       
        /*가을축제에 관한 버튼 * */
        final Button button7 = (Button)findViewById(R.id.button7);   //Sub3fall1Activity (정선아리랑제 축제)정보로 이동하기 위한 버튼
        fallcount++;
        if(flag.sub6_fall1==0)
            button7.setBackgroundResource(R.drawable.where);
           else
        	   button7.setBackgroundResource(R.drawable.changewhere);
        button7.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=36.9816489;
      		     MapActivity.lng=127.9366799;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub6Activity.this);
        		bld.setTitle("충주사과축제");
        	   bld.setMessage("「충주하면 사과, 사과하면 충주」라는 캐치프레이즈로 충주사과의 홍보와 지역경제활성화를 목적으로 1997년부터 매년 10월에 열린다. 사과관련게임, 연예인 축하공연, 품바공연 등 이색적인 행사로 찾은 이들을 즐겁게 하고 사과식품전시회와 품평회 및 사과장터가 열려 값싸고 품질 좋은 사과를 구할 수 있다. 장소충북 충주시 충주체육관 광장기간2013.11.02~2013.11.03");
        		
        	 bld.setIcon(R.drawable.sub6_fall1);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub6Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button7.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub6_fall1=1;
      	         }
           });
        	 bld.show();
             }
        });
      
        
      final  Button button8 = (Button)findViewById(R.id.button8);   //Sub3fall2Activity (양양송이축제)정보로 이동하기 위한 버튼
        fallcount++; 
        if(flag.sub6_fall2==0)
            button8.setBackgroundResource(R.drawable.where);
           else
        	   button8.setBackgroundResource(R.drawable.changewhere);
        button8.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=36.7241845;
      		     MapActivity.lng=127.4406124;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub6Activity.this);
        		bld.setTitle("청원생명축제");
        	   bld.setMessage("생명이 약동하는 풍요의 고장, 아람마루 청원!에서 펼쳐지는 풍성한 친환경 체험거리와 볼거리!신나고 재미있는 공연과 즐길거리!최고의 명품으로 만든 맛있는 생명 농축산물 먹을거리 ! 그리고 여러분의 건강한 “생명”이 살아 숨쉬는 축제 ! 논, 밭, 산 그대로의 축제장으로 여러분을 초대합니다. 장소충북 청원군 오창과학산업단지 내 송대공원기간2013.09.27~2013.10.06");
        		
        	 bld.setIcon(R.drawable.sub6_fall2);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub6Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button8.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub6_fall2=1;
      	         }
           });
        	 bld.show();
             }
        });
        

     final Button button9 = (Button)findViewById(R.id.button9);   //Sub3fall2Activity (양양송이축제)정보로 이동하기 위한 버튼
        fallcount++; 
        if(flag.sub6_fall3==0)
            button9.setBackgroundResource(R.drawable.where);
           else
        	   button9.setBackgroundResource(R.drawable.changewhere);
        button9.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=37.0742716;
      		     MapActivity.lng=127.9679619;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub6Activity.this);
        		bld.setTitle("충주천등산고구마축제");
        	   bld.setMessage("하늘길의 청정자연환경에서 무공해로 재배된 천등산고구마는 육질이 단단하고 단맛이 풍부할 뿐 아니라 점질, 분질이 높아 전국 최고의 효능이 뛰어난 건강 장수식품으로 각광을 받고 있습니다. 장소충북 충주시 산척면사무소 광장 등기간2013.09.07~2013.09.07");
        		
        	 bld.setIcon(R.drawable.sub6_fall3);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub6Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button9.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub6_fall3=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        /*겨울축제에 관한 버튼 * */
        
        /*봄, 여름, 가을, 겨울 구분하기 위한 탭 생성* */
        TabHost tabHost = (TabHost)findViewById(R.id.tabHost);
        tabHost.setup();
     
        TabHost.TabSpec spec = tabHost.newTabSpec("tag1"); //봄을 나타내는 tab1
       spec.setContent(R.id.tab1);
       spec.setIndicator("", this.getResources().getDrawable(R.drawable.tab1));   	      	
       tabHost.addTab(spec);

        
        spec = tabHost.newTabSpec("tag2");  //여름을 나타내는 tab2
        spec.setContent(R.id.tab2);
        spec.setIndicator("", this.getResources().getDrawable(R.drawable.tab2));   	      	
        tabHost.addTab(spec);
        
        
        spec = tabHost.newTabSpec("tag3"); //가을을 나타내는 tab3
        spec.setContent(R.id.tab3);
        spec.setIndicator("", this.getResources().getDrawable(R.drawable.tab3));   	      	 
        tabHost.addTab(spec);
        
        spec = tabHost.newTabSpec("tag5"); //겨울을 나타내는 tab4
        spec.setContent(R.id.tab5);
        spec.setIndicator("", this.getResources().getDrawable(R.drawable.tab4));   	      	
        tabHost.addTab(spec);
        
     
        tabHost.setCurrentTab(0);        
         }
       }
}
