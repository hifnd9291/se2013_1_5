/** 
 * Title: Festival navigation
* @autor : kyjhg1@naver.com
* Last Update : 2013/05/26
* version 4.0
*/ 
package com.example.ftnavi;

import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;
import android.widget.TabHost;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

/*activity_sub5을 위한 클래스(수도권)* */

public class Sub5Activity extends Activity 
{
	private int springcount=0; 
	private int summercount=0; 
	private int fallcount=0;   
    private int wintercount=0; 

    /** Called when the activity is first created. */
    @Override
    /*
    @param :activity_sub5을 위한 메소드
    * */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_sub5);
         {
        	 
        	  /*봄축제에 관한 버튼* */
        	 /*클릭하면 축제의 정보보여주기위한 대화상자가 나타난다* */
       final Button button1 = (Button)findViewById(R.id.button1);  //Sub3spring1Activity (양구곰취축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub5_spirng1==0)
            button1.setBackgroundResource(R.drawable.where);
           else
        	   button1.setBackgroundResource(R.drawable.changewhere);
        button1.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=37.2018197;
      		     MapActivity.lng=126.7911943;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub5Activity.this);
        		bld.setTitle("경기화성세계공룡대축제 ");
        	   bld.setMessage("화성시 송산면 고정리 480만평은 세계적인 중생대 공룡알 화석산지입니다. 또한 문화제 제414호로써, 1999년 최종인씨에 의해 발견된 귀중한 자연유산입니다. 화성시 서부지역 일부 등은 공룡들이 알을 낳고 서식한 산지로서 대단한 문화적 가치가 있습니다. 장소 경기 화성시 두곡리 495-20  기간 2013.05.03~2013.06.23");
        		
        	 bld.setIcon(R.drawable.sub5_spring1);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
                	Intent intent = new Intent(Sub5Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button1.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub5_spirng1=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
        
        final Button button2 = (Button)findViewById(R.id.button2);  //Sub3spring2Activity (백두대간 내면 나물축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub5_spirng2==0)
            button2.setBackgroundResource(R.drawable.where);
           else
        	   button2.setBackgroundResource(R.drawable.changewhere);
        button2.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=37.2933272;
      		     MapActivity.lng=127.2013219;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub5Activity.this);
        		bld.setTitle("에버랜드 장미축제");
        	   bld.setMessage("장소 경기 용인시 에버랜드 기간 2013.05.10~2013.06.16");
        	 bld.setIcon(R.drawable.sub5_spring2);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub5Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button2.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub5_spirng2=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
       final Button button3 = (Button)findViewById(R.id.button3);  //Sub3spring3Activity (커피나무축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub5_spirng3==0)
            button3.setBackgroundResource(R.drawable.where);
           else
        	   button3.setBackgroundResource(R.drawable.changewhere);
        button3.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=36.9909701;
      		     MapActivity.lng=127.1950207;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub5Activity.this);
        		bld.setTitle("호밀밭·초원축제 ");
        	   bld.setMessage("호밀은 가을에 파종하여 싹을 틔워 추운 겨울을 납니다. 척박한 환경 속에서 파릇파릇한 잎사귀를 유지하고 이른 봄 꽃샘 추위에도 아랑곳 하지 않으며 꿋꿋이 줄기를 밀어 올리며 봄의 희망을 전해주는 호밀. 추위를 이기고 알알이 열매 맞는 호밀과 함께 따사로이 시작되는 희망찬 봄을 드넓은 안성팜랜드 초원 위에서 만끽하시기 바랍니다. 장소 경기 안성시 안성팜랜드 기간 2013.04.05~2013.06.30");	
        	 bld.setIcon(R.drawable.sub5_spring3);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub5Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button3.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub5_spirng3=1;
      	         }
           });
        	 bld.show();
             }
        });
        
      final  Button button4 = (Button)findViewById(R.id.button4);  //Sub3spring3Activity (커피나무축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub5_spirng4==0)
            button4.setBackgroundResource(R.drawable.where);
           else
        	   button4.setBackgroundResource(R.drawable.changewhere);
        button4.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=37.5731767;
      		     MapActivity.lng=126.6481326;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub5Activity.this);
        		bld.setTitle("드림파크 야생화단지 개방 축제 ");
        	   bld.setMessage("장소 인천 서구 녹색바이오단지 일원 기간 2013.05.16~2013.05.26");
        	 bld.setIcon(R.drawable.sub5_spring4);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub5Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button4.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub5_spirng4=1;
      	         }
           });
        	 bld.show();
             }
        });
        
       final Button button5 = (Button)findViewById(R.id.button5);  //Sub3spring3Activity (커피나무축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub5_spirng5==0)
            button5.setBackgroundResource(R.drawable.where);
           else
        	   button5.setBackgroundResource(R.drawable.changewhere);
        button5.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=37.7450816;
      		     MapActivity.lng=126.431842;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub5Activity.this);
        		bld.setTitle("고려산 진달래 축제 ");
        	   bld.setMessage("장소 인천 강화군 고려산 일원 기간 2013.04.23~2013.05.05");	
        	 bld.setIcon(R.drawable.sub5_spring5);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub5Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button5.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub5_spirng5=1;
      	         }
           });
        	 bld.show();
             }
        });
        
       final Button button6 = (Button)findViewById(R.id.button6);  //Sub3spring3Activity (커피나무축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub5_spirng6==0)
            button6.setBackgroundResource(R.drawable.where);
           else
        	   button6.setBackgroundResource(R.drawable.changewhere);
        button6.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=37.47523;
      		     MapActivity.lng=126.6215161;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub5Activity.this);
        		bld.setTitle("인천-중국문화관광페스티벌 축제 ");
        	   bld.setMessage("장소 인천 중구 자유공원 등 기간 2013.05.03~2013.05.05");
        	 bld.setIcon(R.drawable.sub5_spring6);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub5Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button6.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub5_spirng6=1;
      	         }
           });
        	 bld.show();
             }
        });
        
       final Button button7 = (Button)findViewById(R.id.button7);  //Sub3spring3Activity (커피나무축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub5_spirng7==0)
            button7.setBackgroundResource(R.drawable.where);
           else
        	   button7.setBackgroundResource(R.drawable.changewhere);
        button7.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=37.5477069;
      		     MapActivity.lng=127.0209074;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub5Activity.this);
        		bld.setTitle("옥수문화축제 ");
        	   bld.setMessage("지역주민 화합의 장, 문화체험의 기회, 공간과 나눔의 마당. 다양한 주민들이 섞여 살아가는 아름다운 우리 동네! 우리 마을에서 살아가는 것에 대한 즐거움과 의미를 함께 찾아보는 소중한 기회를 만듭니다. 가족의 달 5월을 맞이하여 올해로 4회째를 맞이하는 2013 옥수문화축제 <꽃보다 당신-끼페스티발>을 개최하오니 많은 관심과 참여바랍니다. 장소 서울 성동구 옥수역광장 기간 2013.05.24~2013.05.24");
        	 bld.setIcon(R.drawable.sub5_spring7);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub5Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button7.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub5_spirng7=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        final Button button8 = (Button)findViewById(R.id.button8);  //Sub3spring3Activity (커피나무축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub5_spirng8==0)
            button8.setBackgroundResource(R.drawable.where);
           else
        	   button8.setBackgroundResource(R.drawable.changewhere);
        button8.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=37.5704149;
      		     MapActivity.lng=126.979683;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub5Activity.this);
        		bld.setTitle("연등회 연등축제  ");
        	   bld.setMessage("시민의 축제를 위한 새로운 시도들이 이루어졌습니다. 동자승과 부처님 캐릭터 개발과 보급으로 친근함을 주었고 거리문화축제(불교문화마당)과 회향한마당 등을 통해 참여와 호응을 높이고 시민들, 외국인들이 구경꾼이 아니라 참여자로서 축제를 즐기도록 마련해 주었습니다. 장소 서울 종로구 종로거리 기간 2013.05.10~2013.05.12");
        	 bld.setIcon(R.drawable.sub5_spring8);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub5Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button8.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub5_spirng8=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
        
        /*여름축제에 관한 버튼 * */	 
        final Button button9 = (Button)findViewById(R.id.button9);  //Sub3summer1Activity (의암제)정보로 이동하기 위한 버튼
        summercount++;
        if(flag.sub5_summer1==0)
            button9.setBackgroundResource(R.drawable.where);
           else
        	   button9.setBackgroundResource(R.drawable.changewhere);
        button9.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=37.3520199;
      		     MapActivity.lng=126.9822973;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub5Activity.this);
        		bld.setTitle("의왕단오 축제 ");
        	   bld.setMessage("단오는 설날, 추석과 함께 우리나라 3대 명절로 1년 중 양기가 가장 왕성한 날이라 하여 어른, 어린이 모두가 함께 즐길 수 있는 다양한 풍속이 전해져 오는 날이다. 전통체험행사와 다채로운 공연, 단오아줌마 선발대회, 씨름대회, 창포물에 머리감기 등 사라지는 전통문화를 재현하는 전통축제로 시민들과 가족단위 참여객들의 호응이 좋은 축제이다. 장소 경기 의왕시 고천체육공원 기간 2013.06.08~2013.06.08");
        	 bld.setIcon(R.drawable.sub5_summer1);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub5Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button9.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub5_summer1=1;
      	         }
           });
        	 bld.show();
             }
        });
        
       
        
        final Button button10 = (Button)findViewById(R.id.button10);  //Sub3summer2Activity (원주 장미축제)정보로 이동하기 위한 버튼
        summercount++;
        if(flag.sub5_summer2==0)
            button10.setBackgroundResource(R.drawable.where);
           else
        	   button10.setBackgroundResource(R.drawable.changewhere);
        button10.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=37.4592543;
      		     MapActivity.lng=127.3080375;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub5Activity.this);
        		bld.setTitle("퇴촌토마토축제 ");
        	   bld.setMessage("장소 경기 광주시 정지리 행사장 기간 2013.06.21~2013.06.23");
        	 bld.setIcon(R.drawable.sub5_summer2);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub5Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button10.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub5_summer2=1;
      	    	}
           });
        	 bld.show();
             }
        });
        
     
       final Button button11= (Button)findViewById(R.id.button11);  //Sub3summer3Activity (강릉단오제)정보로 이동하기 위한 버튼
        summercount++;
        if(flag.sub5_summer3==0)
            button11.setBackgroundResource(R.drawable.where);
           else
        	   button11.setBackgroundResource(R.drawable.changewhere);
        button11.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=37.3907492;
      		     MapActivity.lng=127.5338765;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub5Activity.this);
        		bld.setTitle("금사참외축제 ");
        	   bld.setMessage("장소 경기 여주군 금사근린공원 일원 기간 2013.05.31~2013.06.02");	
        	 bld.setIcon(R.drawable.sub5_summer3);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub5Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button11.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub5_summer3=1;
      	         }
           });
        	 bld.show();
             }
        });
       
        /*:가을축제에 관한 버튼* */
       final Button button12 = (Button)findViewById(R.id.button12);   //Sub3fall1Activity (정선아리랑제 축제)정보로 이동하기 위한 버튼
        fallcount++;
        if(flag.sub5_fall1==0)
            button12.setBackgroundResource(R.drawable.where);
           else
        	   button12.setBackgroundResource(R.drawable.changewhere);
        button12.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=37.582604;
      		     MapActivity.lng=126.9919377;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub5Activity.this);
        		bld.setTitle("창덕궁 달빛기행 축제 ");
        	   bld.setMessage("살아숨쉬는 궁궐만들기의 일환으로 특별한 시간대에 궁궐에서 특별한 경험의 기회가 여러분을 기다립니다. 매월 음력 보름! 보름달과 창덕궁의 아름다움을 느껴보세요. 장소 서울 종로구 창덕궁 일원 기간 2013.03.26~2013.10.20");
        	 bld.setIcon(R.drawable.sub5_fall1);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub5Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button12.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub5_fall1=1;
      	         }
           });
        	 bld.show();
             }
        });
      
        
       final Button button13 = (Button)findViewById(R.id.button13);   //Sub3fall2Activity (양양송이축제)정보로 이동하기 위한 버튼
        fallcount++; 
        if(flag.sub5_fall2==0)
            button13.setBackgroundResource(R.drawable.where);
           else
        	   button13.setBackgroundResource(R.drawable.changewhere);
        button13.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=37.2869491;
      		     MapActivity.lng=127.0118167;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub5Activity.this);
        		bld.setTitle("수원화성문화제 축제 ");
        	   bld.setMessage("매년 10월 개최되는 '수원화성문화제'는 조선조 제22대 임금 정조대왕의 지극한 효심과 개혁에 대한 꿈으로 축성된 세계문화유산 [수원화성]을 무대로 펼쳐지는 대한민국에서 하나뿐인 성곽축제입니다. 정조대왕의 을묘년 화성원행을 재현하는 정조대왕 능행차와 친림과거시험, 혜경궁홍씨 진찬연 등 다양한 전통행사 재현과 정조대왕의 개혁사상과 [수원화성]을 배경으로 한 총체공연, 수원의 아름다운 생태하천 '수원천'에서 펼쳐지는 공공예술프로젝트 등 국내외 관광객들의 관심과 사랑을 받는 대한민국 대표 전통문화관광축제입니다. 장소 경기 수원시 화성행궁 광장 등 기간 2013.09.27~2013.10.01");
        	 bld.setIcon(R.drawable.sub5_fall2);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub5Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button13.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub5_fall2=1;
      	         }
           });
        	 bld.show();
             }
        });
        
       final Button button14 = (Button)findViewById(R.id.button14);   //Sub3fall2Activity (양양송이축제)정보로 이동하기 위한 버튼
        fallcount++; 
        if(flag.sub5_fall3==0)
            button14.setBackgroundResource(R.drawable.where);
           else
        	   button14.setBackgroundResource(R.drawable.changewhere);
        button14.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {
            	 MapActivity.lat=37.2769723;
      		     MapActivity.lng=127.4270634;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub5Activity.this);
        		bld.setTitle("이천쌀 문화축제 ");
        	   bld.setMessage("축제설명알차게 여문 햅쌀을 직접 구입할 수 있으며, 어린 세대에겐 전통 농경문화를 체험케 하고, 어른들에겐 옛 향수를 자아내면서 함께 어울릴 수 있는 축제의 한마당에 당신을 초대합니다. 특히, 이천쌀문화축제는 문화관광부에서 지원하는 축제로서 더욱 알차고 흥겨운 축제가 될 것 입니다. 장소: 경기 이천시 설봉공원  기간:2013.10.30~2013.11.03");
        	 bld.setIcon(R.drawable.sub5_fall3);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub5Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button14.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub5_fall3=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
        /*겨울축제에 관한 버튼* */
        final Button button15 = (Button)findViewById(R.id.button15);   //Sub3winter1Activity (태백산눈꽃축제)정보로 이동하기 위한 버튼
        wintercount++;
        if(flag.sub5_winter1==0)
            button15.setBackgroundResource(R.drawable.where);
           else
        	   button15.setBackgroundResource(R.drawable.changewhere);
        button15.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=37.5313524;
      		     MapActivity.lng=126.9703782;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub5Activity.this);
        		bld.setTitle("국제캠핑페어 ");
        	   bld.setMessage("장소: 경기 고양시 일산 킨텍스(KINTEX)기간 : 2014.02.27~2014.03.02");
        	 bld.setIcon(R.drawable.sub5_winter1);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub5Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button15.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub5_winter1=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
       final Button button16 = (Button)findViewById(R.id.button16);  //Sub3winter2Activity (홍천강 꽁꽁축제 )정보로 이동하기 위한 버튼
        wintercount++;
        if(flag.sub5_winter2==0)
            button16.setBackgroundResource(R.drawable.where);
           else
        	   button16.setBackgroundResource(R.drawable.changewhere);
        button16.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=37.5180443;
      		     MapActivity.lng=127.0153972;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub5Activity.this);
        		bld.setTitle("서울디자인페스티벌");
        	   bld.setMessage("서울디자인페스티벌은 1976년부터 국내외 디자인 분야의 메신저 역할을 해 온 월간 <디자인>이 주관하는 디자인 전문 전시회입니다. 트렌드를 선도하는 디자인경영 브랜드, 디자인전문회사, 역량 있는 디자이너들이 제시하는 디자인솔루션이 매년 소개되며 디자이너, 마케터, 트렌드 리서치 기관, 문화계 인사, 전공생 등 연간 5만명의 참관객 들이 방문하는 대한민국의 대표 디자인 축제입니다. 장소서울 강남구 코엑스(COEX)기간2013.12.18~2013.12.22");
        	 bld.setIcon(R.drawable.sub5_winter2);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub5Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button16.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub5_winter2=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
        /*봄, 여름, 가을, 겨울 구분하기 위한 탭 생성   * */
        TabHost tabHost = (TabHost)findViewById(R.id.tabHost);
        tabHost.setup();
     
        TabHost.TabSpec spec = tabHost.newTabSpec("tag1"); //봄을 나타내는 tab1
       spec.setContent(R.id.tab1);
       spec.setIndicator("", this.getResources().getDrawable(R.drawable.tab1));   	      	
       tabHost.addTab(spec);

        
        spec = tabHost.newTabSpec("tag2");  //여름을 나타내는 tab2
        spec.setContent(R.id.tab2);
        spec.setIndicator("", this.getResources().getDrawable(R.drawable.tab2));   	      	
        tabHost.addTab(spec);
        
        
        spec = tabHost.newTabSpec("tag3"); //가을을 나타내는 tab3
        spec.setContent(R.id.tab3);
        spec.setIndicator("", this.getResources().getDrawable(R.drawable.tab3));   	      	 
        tabHost.addTab(spec);
        
        spec = tabHost.newTabSpec("tag5"); //겨울을 나타내는 tab4
        spec.setContent(R.id.tab5);
        spec.setIndicator("", this.getResources().getDrawable(R.drawable.tab4));   	      	
        tabHost.addTab(spec);
        
     
        tabHost.setCurrentTab(0);        
         }
       }
}