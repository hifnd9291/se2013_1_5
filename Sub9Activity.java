/** 
 * Title: Festival navigation
* @autor : kyjhg1@naver.com
* Last Update : 2013/05/26
* version 4.0
*/ 
package com.example.ftnavi;

import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;
import android.widget.TabHost;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

/*activity_sub9을 위한 클래스(경상북도)* */

public class Sub9Activity extends Activity 
{
	private int springcount=0; 
	private int summercount=0; 
	private int fallcount=0;  
    private int wintercount=0; 

    /** Called when the activity is first created. */
    @Override
    /*
    @param :activity_sub9을 위한 메소드
    * */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_sub9);
         {
        	 
        	  /*봄축제에 관한 버튼 * */
        	 /*:클릭하면 축제의 정보보여주기위한 대화상자가 나타난다* */
        final Button button1 = (Button)findViewById(R.id.button1);  //Sub3spring1Activity (양구곰취축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub9_spirng1==0)
            button1.setBackgroundResource(R.drawable.where);
           else
        	   button1.setBackgroundResource(R.drawable.changewhere);
       button1.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=36.7607246;
      		     MapActivity.lng=128.0770262;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub9Activity.this);
        		bld.setTitle("문경새재 옛길달빛사랑여행 축제");
        	   bld.setMessage("백두대간 명산과 전통찻사발의 본고장 문경! 기쁜소식을 듣는 고장 문경에서 과거길과 달빛 그리고 사랑을 주제한 [문경새재 옛길달빛사랑여행] 행사를 개최합니다. 조선시대 영남에서 한영으로 통하는 가장 큰길인 문경새재로 여러분을 정중히 초대 하오니 많이 오셔서 나들이의 낭만과 가족, 연인, 친구 사랑을 확인하는 좋은 만남의 시간을 가지시기 바랍니다. 장소경북 문경시 문경새재기간2013.05.25~2013.09.19");
        		
        	 bld.setIcon(R.drawable.sub9_spring1);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
                	Intent intent = new Intent(Sub9Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button1.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub9_spirng1=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
        
        final Button button2 = (Button)findViewById(R.id.button2);  //Sub3spring2Activity (백두대간 내면 나물축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub9_spirng2==0)
            button2.setBackgroundResource(R.drawable.where);
           else
        	   button2.setBackgroundResource(R.drawable.changewhere);
        button2.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=35.7592532;
      		     MapActivity.lng=128.6300401; 
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub9Activity.this);
        		bld.setTitle("청도 프로방스 러브러브 빛축제  ");
        	   bld.setMessage("빛으로 전하는 로맨틱한 사랑이야기를 주제로 펼쳐지는 빛의 축제!! 청도 프로방스는 어둠이 내리면 더욱 화려하게 변신을 합니다. 숲 속 가득히 채운 눈부신 빛에 흠뻑 빠져 보세요~! 장소경북 청도군 청도프로방스포토랜드기간2012.12.21~9999.12.31");
        		
        	 bld.setIcon(R.drawable.sub9_spring2);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub9Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button2.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub9_spirng2=1;
      	         }
           });
        	 bld.show();
        	 
             }
        });
        
        
       final Button button3 = (Button)findViewById(R.id.button3);  //Sub3spring3Activity (커피나무축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub9_spirng3==0)
            button3.setBackgroundResource(R.drawable.where);
           else
        	   button3.setBackgroundResource(R.drawable.changewhere);
        button3.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=36.1312927;
      		     MapActivity.lng=129.1669142; 
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub9Activity.this);
        		bld.setTitle("기북 산나물 축제 ");
        	   bld.setMessage("맑은 공기와 자연의 순수함을 고이 간직하고 있는 청정 기북면에서 주민들이 정성을 모아 채취한 무공해 산나물과 지역특산품을 마련하여「제14회 기북산나물 축제」를 준비하고 있습니다. 축제행사에 많은 시민들의 참여 부탁드립니다. 장소경북 포항시 기계중학교기북분교 운동장기간2013.05.11~2013.05.119");
        		
        	 bld.setIcon(R.drawable.sub9_spring3);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub9Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button3.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub9_spirng3=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
        /*여름축제에 관한 버튼* */	 
      final  Button button4 = (Button)findViewById(R.id.button4);  //Sub3summer1Activity (의암제)정보로 이동하기 위한 버튼
        summercount++;
        if(flag.sub9_summer1==0)
            button4.setBackgroundResource(R.drawable.where);
           else
        	   button4.setBackgroundResource(R.drawable.changewhere);
        //button4.setPadding(10, 10, 10, 10);
        button4.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=36.7782029;
      		     MapActivity.lng=128.1101229;  
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub9Activity.this);
        		bld.setTitle("장기 산딸기 문화축제");
        	   bld.setMessage("경북 포항시 장기초등학교 운동장기간2013.06.08~2013.06.08");
        		
        	 bld.setIcon(R.drawable.sub9_summer1);
        	
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub9Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button4.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub9_summer1=1;
      	         }
           });
        	 bld.show();
             }
        });
        
       
        
       final Button button5 = (Button)findViewById(R.id.button5);  //Sub3summer2Activity (원주 장미축제)정보로 이동하기 위한 버튼
       // button5.setPadding(20, 20, 20, 20);
        summercount++;
        if(flag.sub9_summer2==0)
            button5.setBackgroundResource(R.drawable.where);
           else
        	   button5.setBackgroundResource(R.drawable.changewhere);
        button5.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=35.8465151;
      		     MapActivity.lng=129.2850097;  
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub9Activity.this);
        		bld.setTitle("경주 천년의 사랑 불빛축제");
        	   bld.setMessage("세계최초 국내최대 규모인 3000여 평의 540만개 LED불꽃전시 경주특별전으로 제작한 경주 천년의 사랑,천년의 불꽃 축제는 믿음, 소망, 사랑과 자비로 주제가 담긴 드림아트 전시관으로서 국내최고의 엔지니어와 장비로 이루어낸 장소경북 경주시 경주조선온천호텔기간2012.08.24~9999.12.31");
        		
        	 bld.setIcon(R.drawable.sub3_summer2);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub9Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button5.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub9_summer2=1;
      	         }
           });
        	 bld.show();
             }
        });
        
     
        final Button button6 = (Button)findViewById(R.id.button6);  //Sub3summer3Activity (강릉단오제)정보로 이동하기 위한 버튼
        summercount++;
        if(flag.sub9_summer3==0)
            button6.setBackgroundResource(R.drawable.where);
           else
        	   button6.setBackgroundResource(R.drawable.changewhere);
        button6.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=36.5633496;
      		     MapActivity.lng=128.2981004;  
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub9Activity.this);
        		bld.setTitle("예천 삼강주막 막걸리축제");
        	   bld.setMessage("삼강주막은 삼강나루의 나들이객에게 허기를 면하게 해주고 보부상들의 숙식처로 때론 시인 묵객들의 유상처로 이용된 건물이다. 이런 주막의 서정, 주모의 인정을 막걸리와 함께 즐길수 있는 삼강주막막걸리축제가 매년 7월말~8월초 예천에서 개최된다. 막걸리 전시 및 판매, 공연 및 레크레이션, 체험행사 등 다양한 프로그램을 진행하여 먹고, 보고, 체험하며 즐길수 있는 축제의 장을 마련한다. 장소경북 예천군 삼강주막기간2013.08.02~2013.08.04");
        		
        	 bld.setIcon(R.drawable.sub9_summer3);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub9Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button6.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub9_summer3=1;
      	         }
           });
        	 bld.show();
             }
        });
       
        /*가을축제에 관한 버튼* */
        final Button button7 = (Button)findViewById(R.id.button7);   //Sub3fall1Activity (정선아리랑제 축제)정보로 이동하기 위한 버튼
        fallcount++;
        if(flag.sub9_fall1==0)
            button7.setBackgroundResource(R.drawable.where);
           else
        	   button7.setBackgroundResource(R.drawable.changewhere);
        button7.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=36.8692018;
      		     MapActivity.lng=128.5218953;  
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub9Activity.this);
        		bld.setTitle("영주 풍기인삼축제 ");
        	   bld.setMessage("경상북도 영주시 풍기읍에서 해마다 10월에 열리는 축제이다. 인삼이 건강을 상징하듯이 인삼축제는 웰빙시대를 상징하는 건강 관련 전시ㆍ체험 행사 위주로 진행된다. 뿐만아니라 인삼향 그윽한 축제장에는 볼거리와 즐길거리, 그리고 인삼캐기체험과 먹을거리 등 다양한 프로그램이 준비되어있다. 가족과 함께하는 최고의 웰빙축제로 문화체육관광부지정 우수축제로 뽑히기도 했다. 장소경북 영주시 남원천 둔치기간2013.10.03~2013.10.09전화번호Tel. 054-635-0020");
        		
        	 bld.setIcon(R.drawable.sub9_fall1);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub9Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button7.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub9_fall1=1;
      	         }
           });
        	 bld.show();
             }
        });
      
        
        final Button button8 = (Button)findViewById(R.id.button8);   //Sub3fall2Activity (양양송이축제)정보로 이동하기 위한 버튼
        fallcount++; 
        if(flag.sub9_fall2==0)
            button8.setBackgroundResource(R.drawable.where);
           else
        	   button8.setBackgroundResource(R.drawable.changewhere);
        button8.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=36.5503978;
      		     MapActivity.lng=128.6269816;  
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub9Activity.this);
        		bld.setTitle("안동 국제 탈춤 페스티벌");
        	   bld.setMessage("1997년부터 안동에서 개최 되는 전통 예술 축제이다. 안동탈춤 축제는 하회 별신굿 탈놀이를 중심으로 개최되었다. 또한 경상북도 5대축제 중 하나로 전통문화의 전승과 재현을 통해 문화 시민으로서의 자긍심을 고취하고자 개최된 축제이다. 한국탈춤, 마당극, 안동민속축제, 외국탈춤 등 다양한 공연뿐 아니라 탈만들기, 탈체험, 하회세계탈박물관 등 다양한 체험, 전시, 문화예술행사를 즐길수 있다. 장소경북 안동시 하회마을 등기간2013.09.27~2013.10.06");
        		
        	 bld.setIcon(R.drawable.sub9_fall2);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub9Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button8.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub9_fall2=1;
      	         }
           });
        	 bld.show();
             }
        });
        
      final Button button9 = (Button)findViewById(R.id.button9);   //Sub3fall2Activity (양양송이축제)정보로 이동하기 위한 버튼
        fallcount++; 
        if(flag.sub9_fall3==0)
            button9.setBackgroundResource(R.drawable.where);
           else
        	   button9.setBackgroundResource(R.drawable.changewhere);
        button9.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=36.7607246;
      		     MapActivity.lng=128.0770262;   
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub9Activity.this);
        		bld.setTitle("문경새재 옛길달빛사랑여행");
        	   bld.setMessage("백두대간 명산과 전통찻사발의 본고장 문경! 기쁜소식을 듣는 고장 문경에서 과거길과 달빛 그리고 사랑을 주제한 [문경새재 옛길달빛사랑여행] 행사를 개최합니다. 조선시대 영남에서 한영으로 통하는 가장 큰길인 문경새재로 여러분을 정중히 초대 하오니 많이 오셔서 나들이의 낭만과 가족, 연인, 친구 사랑을 확인하는 좋은 만남의 시간을 가지시기 바랍니다. 장소경북 문경시 문경새재기간2013.05.25~2013.09.19");
        		
        	 bld.setIcon(R.drawable.sub9_fall3);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub9Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button9.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub9_fall3=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
        /*겨울축제에 관한 버튼* */
       final Button button10 = (Button)findViewById(R.id.button10);   //Sub3winter1Activity (태백산눈꽃축제)정보로 이동하기 위한 버튼
        wintercount++;
        if(flag.sub9_winter1==0)
            button10.setBackgroundResource(R.drawable.where);
           else
        	   button10.setBackgroundResource(R.drawable.changewhere);
        button10.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=35.6843018;
      		     MapActivity.lng=128.7181727;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub9Activity.this);
        		bld.setTitle("청도 프로방스 러브러브 빛축제");
        	   bld.setMessage("빛으로 전하는 로맨틱한 사랑이야기를 주제로 펼쳐지는 빛의 축제!! 청도 프로방스는 어둠이 내리면 더욱 화려하게 변신을 합니다. 숲 속 가득히 채운 눈부신 빛에 흠뻑 빠져 보세요~! 장소경북 청도군 청도프로방스포토랜드기간2012.12.21~9999.12.31");
        		
        	 bld.setIcon(R.drawable.sub9_winter1);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub9Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button10.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub9_winter1=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
        final Button button11 = (Button)findViewById(R.id.button11);  //Sub3winter2Activity (홍천강 꽁꽁축제 )정보로 이동하기 위한 버튼
        wintercount++;
        if(flag.sub9_winter2==0)
            button11.setBackgroundResource(R.drawable.where);
           else
        	   button11.setBackgroundResource(R.drawable.changewhere);
        button11.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=35.8465151;
      		     MapActivity.lng=129.2850097;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub9Activity.this);
        		bld.setTitle("경주 천년의 사랑 불빛축제");
        	   bld.setMessage("세계최초 국내최대 규모인 3000여 평의 540만개 LED불꽃전시 경주특별전으로 제작한 경주 천년의 사랑천년의 불꽃 축제는 믿음, 소망, 사랑과 자비로 주제가 담긴 드림아트 전시관으로서 국내최고의 엔지니어와 장비로 이루어낸 장소경북 경주시 경주조선온천호텔기간2012.08.24~9999.12.31");
        		
        	 bld.setIcon(R.drawable.sub9_winter2);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub9Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button11.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub9_winter2=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
        /*봄, 여름, 가을, 겨울 구분하기 위한 탭 생성* */
        TabHost tabHost = (TabHost)findViewById(R.id.tabHost);
        tabHost.setup();
     
        TabHost.TabSpec spec = tabHost.newTabSpec("tag1"); //봄을 나타내는 tab1
       spec.setContent(R.id.tab1);
       spec.setIndicator("", this.getResources().getDrawable(R.drawable.tab1));   	      	
       tabHost.addTab(spec);

        
        spec = tabHost.newTabSpec("tag2");  //여름을 나타내는 tab2
        spec.setContent(R.id.tab2);
        spec.setIndicator("", this.getResources().getDrawable(R.drawable.tab2));   	      	
        tabHost.addTab(spec);
        
        
        spec = tabHost.newTabSpec("tag3"); //가을을 나타내는 tab3
        spec.setContent(R.id.tab3);
        spec.setIndicator("", this.getResources().getDrawable(R.drawable.tab3));   	      	 
        tabHost.addTab(spec);
        
        spec = tabHost.newTabSpec("tag5"); //겨울을 나타내는 tab4
        spec.setContent(R.id.tab5);
        spec.setIndicator("", this.getResources().getDrawable(R.drawable.tab4));   	      	
        tabHost.addTab(spec);
        
     
        tabHost.setCurrentTab(0);        
         }
       }
}
