/** 
 * Title: Festival navigation
* @autor : kyjhg1@naver.com
* Last Update : 2013/05/26
* version 4.0
*/ 
package com.example.ftnavi;

import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;
import android.widget.TabHost;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

/*
         레이아웃 activity_sub10을 위한 클래스
        경상남도에 대한 축제를 보여주기위한 클래스
* */
public class Sub10Activity extends Activity 
{
	private int springcount=0; 
	private int summercount=0; 
	private int fallcount=0;   
    private int wintercount=0; 

    /** Called when the activity is first created. */
    @Override
    /*
    @param :activity_sub10을 위한 메소드
    * */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_sub10);
         {
        	 
        	  /*  봄축제에 관한 버튼 * */
        	 /*클릭하면 축제의 정보보여주기위한 대화상자가 나타난다 * */
       final Button button1 = (Button)findViewById(R.id.button1);  //Sub3spring1Activity (양구곰취축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub10_spirng1==0)
            button1.setBackgroundResource(R.drawable.where);
           else
        	   button1.setBackgroundResource(R.drawable.changewhere);
        button1.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {	
            	 //해당 축제의 위도와 경도 지정
            	 MapActivity.lat=35.478873;
           		MapActivity.lng=127.9971317;
           		
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub10Activity.this);
        		bld.setTitle("합천 황매산철쭉제");
        	   bld.setMessage("철쭉 군락지인 정상 바로 아래는 과거 목장을 조성했던 평원으로 구릉진 초원이 이국적인 풍경을 자아내며, 황량한 겨울을 이겨낸 초목과 붉은 꽃의 조화가 끝없이 펼쳐진 산상화원의 모습이야말로 황매산 철쭉 산행의 백미입니다. 또한 철쭉군락지 초입까지 찻길이 나 있어 자동차로 편하게 접근할 수 있고, 철쭉군락지로 향하는 길도 목재데크 등으로 넓고 편하게 조성돼있어 아이들 또는 노부모를 동반한 가족 산행 코스로 제격입니다. 장소경남 합천군 황매산군립공원 철쭉군락지기간2013.05.14~2013.05.24");
        		
        	 bld.setIcon(R.drawable.sub10_spring1);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
                	Intent intent = new Intent(Sub10Activity.this, MapActivity.class); //구굴 지도 클래스로 넘어간다.
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button1.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub10_spirng1=1; //해당 플래그를 1로 바꾸워서 1일 때 해당하는 파란색버튼으로 이미지를 변경한다.
      	         }
           });
        	 bld.show();
             }
        });
        
        
        
        final Button button2 = (Button)findViewById(R.id.button2);  //Sub3spring2Activity (백두대간 내면 나물축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub10_spirng2==0)
            button2.setBackgroundResource(R.drawable.where);
           else
        	   button2.setBackgroundResource(R.drawable.changewhere);
        button2.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {	
            	 MapActivity.lat=35.1067187;
            		MapActivity.lng=128.7347163;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub10Activity.this);
        		bld.setTitle("진해구 웅천동 유채꽃 축제 축제");
        	   bld.setMessage("올해 유채는 높은 기온과 적절한 봄비가 더해져 어느 해보다도 작황이 좋고 꽃줄기가 힘차게 뻗어 올랐다. 축제위원회는 오는 13일이면 유채가 만개할 것으로 내다보고 있다.유채축제는 난타공연, 스포츠댄스, 색소폰 연주 등 다채로운 문화행사와 함께 먹거리장터 도 운영된다. 장소경남 창원시 제덕만 일원기간2013.04.13~2013.04.17");
        		
        	 bld.setIcon(R.drawable.sub10_spring2);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub10Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button2.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub10_spirng2=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
       final Button button3 = (Button)findViewById(R.id.button3);  //Sub3spring3Activity (커피나무축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub10_spirng3==0)
            button3.setBackgroundResource(R.drawable.where);
           else
        	   button3.setBackgroundResource(R.drawable.changewhere);
        button3.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=35.3846111;
         		MapActivity.lng=128.471836;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub10Activity.this);
        		bld.setTitle(" 낙동강유채축제");
        	   bld.setMessage("창녕군 남지읍 낙동강변 광활한 둔치에 유채단지를 조성하여 남지체육공원에서 우리고장의 안녕과 번영을 비는 낙동강용왕대제를 시작으로 공연행사, 전통행사(낙동강용왕대제), 문화행사, 체육행사 등 다양한 이벤트 행사와 함께 낙동강 유채축제가 개최된다. 장소경남 창녕군 남지유채단지, 남지체육공원 일원기간2013.04.19~2013.04.23");
        		
        	 bld.setIcon(R.drawable.sub10_spring3);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub10Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button3.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub10_spirng3=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
        /*여름축제에 관한 버튼 * */	 
      final  Button button4 = (Button)findViewById(R.id.button4);  //Sub3summer1Activity (의암제)정보로 이동하기 위한 버튼
        summercount++;
        if(flag.sub10_summer1==0)
            button4.setBackgroundResource(R.drawable.where);
           else
        	   button4.setBackgroundResource(R.drawable.changewhere);
        //button4.setPadding(10, 10, 10, 10);
        button4.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {	
            	 MapActivity.lat=34.8142711;
          		MapActivity.lng=127.9197134;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub10Activity.this);
        		bld.setTitle("보물섬 마늘축제");
        	   bld.setMessage("경남 남해군 보물섬마늘나라 일원기간2013.05.30~2013.06.02");
        		
        	 bld.setIcon(R.drawable.sub10_summer1);
        	
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub10Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button4.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub10_summer1=1;
      	         }
           });
        	 bld.show();
             }
        });
        
       
        
       final Button button5 = (Button)findViewById(R.id.button5);  //Sub3summer2Activity (원주 장미축제)정보로 이동하기 위한 버튼
       // button5.setPadding(20, 20, 20, 20);
        summercount++;
        if(flag.sub10_summer2==0)
            button5.setBackgroundResource(R.drawable.where);
           else
        	   button5.setBackgroundResource(R.drawable.changewhere);
        button5.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=36.566877;
           		MapActivity.lng=127.3936545;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub10Activity.this);
        		bld.setTitle("함양산삼축제");
        	   bld.setMessage("함양에 오시면 건강이 있습니다. 여름 휴가 때 함양산삼축제를 찾아옷서 가족과 연인과 함께 건광과 사랑을 나누는 소중한 추억 간직하시기 바랍니다. 장소경남 함양군 함양상림기간2013.08.01~2013.08.05");
        		
        	 bld.setIcon(R.drawable.sub10_summer2);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub10Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button5.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub10_summer2=1;
      	         }
           });
        	 bld.show();
             }
        });
        
     
        final Button button6 = (Button)findViewById(R.id.button6);  //Sub3summer3Activity (강릉단오제)정보로 이동하기 위한 버튼
        summercount++;
        if(flag.sub10_summer3==0)
            button6.setBackgroundResource(R.drawable.where);
           else
        	   button6.setBackgroundResource(R.drawable.changewhere);
        button6.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=34.8452237;
            		MapActivity.lng=128.4239183;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub10Activity.this);
        		bld.setTitle("통영연극예술축제");
        	   bld.setMessage("2013통영연극예술축제는 인간의 근원적 감정 깊숙이 파고들어 그것을 드러내 보이고 희로애락 (喜怒哀樂)의 원형적(原型的) 감정을 표출하고자 한다. 장소경남 통영시 시민문화회관 대극장 등기간2013.07.12~2013.07.20");
        		
        	 bld.setIcon(R.drawable.sub10_summer3);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub10Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button6.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub10_summer3=1;
      	         }
           });
        	 bld.show();
             }
        });
       
        /*가을축제에 관한 버튼 * */
        final Button button7 = (Button)findViewById(R.id.button7);   //Sub3fall1Activity (정선아리랑제 축제)정보로 이동하기 위한 버튼
        fallcount++;
        if(flag.sub10_fall1==0)
            button7.setBackgroundResource(R.drawable.where);
           else
        	   button7.setBackgroundResource(R.drawable.changewhere);
        button7.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {	
            	 MapActivity.lat=35.1881256;
         		MapActivity.lng=128.0867402;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub10Activity.this);
        		bld.setTitle("진주남강유등축제");
        	   bld.setMessage("경상남도 진주에서 매년 10월에 열리는 축제이다. 우리 계레의 최대 수난기였던 임진왜란의 진주성 전투에 기원하는 마음을 한데모아 밝히는 등불 축제이다. 소망등달기, 유등띄우기, 창작등만들기체험, 남가람 어울마당, 수상불꽃놀이 등 다양한 프로그램으로 진행된다. 가장 단기간에 최우수 축제로 선정 되었으며 2012년 대한민국 대표축제로 선정되었다. 장소경남 진주시 남강 일원기간2013.10.01~2013.10.13");
        		
        	 bld.setIcon(R.drawable.sub10_fall1);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub10Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button7.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub10_fall1=1;
      	         }
           });
        	 bld.show();
             }
        });
      
        
        final Button button8 = (Button)findViewById(R.id.button8);   //Sub3fall2Activity (양양송이축제)정보로 이동하기 위한 버튼
        fallcount++; 
        if(flag.sub10_fall2==0)
            button8.setBackgroundResource(R.drawable.where);
           else
        	   button8.setBackgroundResource(R.drawable.changewhere);
        button8.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {	
            	 MapActivity.lat=35.4609881;
          		MapActivity.lng=127.7894231;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub10Activity.this);
        		bld.setTitle("산청한방약초축제 ");
        	   bld.setMessage("지리적인 자연환경과 역사적 배경을 바탕으로 하여 산청군이 전통한방과 약초의 본 고장임을 널리 알려 우리군의 위상을 확립하고, 약초의 한방관련 산업 육성을 통해 지역경제 활성화 및 주민 소득증대를 위한 군민들의 의지결집과 적극적인 동참을 위한 방안으로 축제를 기획하게 되었습니다. 장소경남 산청군 동의보감촌 등기간2013.10.04~2013.10.11");
        		
        	 bld.setIcon(R.drawable.sub10_fall2);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub10Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button8.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub10_fall2=1;
      	         }
           });
        	 bld.show();
             }
        });
        
       final  Button button9 = (Button)findViewById(R.id.button9);   //Sub3fall2Activity (양양송이축제)정보로 이동하기 위한 버튼
        fallcount++; 
        if(flag.sub10_fall3==0)
            button9.setBackgroundResource(R.drawable.where);
           else
        	   button9.setBackgroundResource(R.drawable.changewhere);
        button9.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {	
            	 MapActivity.lat=35.1898826;
           		MapActivity.lng=128.0793255;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub10Activity.this);
        		bld.setTitle("진주개천예술제 축제");
        	   bld.setMessage("개천예술제는 1949년(단기 4282년)에 정부수립의 실질적인 자주독립 1주년을 기리고 예술문화의 발전을 위해서 제1회 영남예술제로 개최되었다. 그 이후 1950년 한국전쟁과 1979년 10.26을 제외하고 매년 어떤 어려움에도 그 맥을 이어온 국내 최대, 최고의 예술제이다. 장소경남 진주시 진주성 등기간2013.10.03~2013.10.10");
        		
        	 bld.setIcon(R.drawable.sub10_fall3);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub10Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button9.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub10_fall3=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
        /*봄, 여름, 가을, 겨울 구분하기 위한 탭 생성 * */
        TabHost tabHost = (TabHost)findViewById(R.id.tabHost);
        tabHost.setup();
     
        TabHost.TabSpec spec = tabHost.newTabSpec("tag1"); //봄을 나타내는 tab1
       spec.setContent(R.id.tab1);
       spec.setIndicator("", this.getResources().getDrawable(R.drawable.tab1));   	      	
       tabHost.addTab(spec);

        
        spec = tabHost.newTabSpec("tag2");  //여름을 나타내는 tab2
        spec.setContent(R.id.tab2);
        spec.setIndicator("", this.getResources().getDrawable(R.drawable.tab2));   	      	
        tabHost.addTab(spec);
        
        
        spec = tabHost.newTabSpec("tag3"); //가을을 나타내는 tab3
        spec.setContent(R.id.tab3);
        spec.setIndicator("", this.getResources().getDrawable(R.drawable.tab3));   	      	 
        tabHost.addTab(spec);
        
        spec = tabHost.newTabSpec("tag5"); //겨울을 나타내는 tab4
        spec.setContent(R.id.tab5);
        spec.setIndicator("", this.getResources().getDrawable(R.drawable.tab4));   	      	
        tabHost.addTab(spec);
        
     
        tabHost.setCurrentTab(0);        
         }
       }
}
