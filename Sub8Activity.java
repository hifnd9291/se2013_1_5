/** 
 * Title: Festival navigation
* @autor : kyjhg1@naver.com
* Last Update : 2013/05/26
* version 4.0
*/  
package com.example.ftnavi;

import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;
import android.widget.TabHost;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

/*activity_sub8을 위한 클래스(전라북도)* */

public class Sub8Activity extends Activity 
{
	private int springcount=0; 
	private int summercount=0; 
	private int fallcount=0;   
    private int wintercount=0; 

    /** Called when the activity is first created. */
    @Override
    /*
    @param :activity_sub8을 위한 메소드
    * */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_sub8);
         {
        	 
        	  /*:봄축제에 관한 버튼 * */
        	 /*클릭하면 축제의 정보보여주기위한 대화상자가 나타난다 * */
       final Button button1 = (Button)findViewById(R.id.button1);  //Sub3spring1Activity (양구곰취축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub8_spirng1==0)
            button1.setBackgroundResource(R.drawable.where);
           else
        	   button1.setBackgroundResource(R.drawable.changewhere);
        button1.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=35.4216723;
      		     MapActivity.lng=127.5762229;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub8Activity.this);
        		bld.setTitle("지리산 운봉바래봉 철쭉제");
        	   bld.setMessage("전북 남원시 바래봉, 허브밸리 일원기간2013.04.27~2013.05.26");
        		
        	 bld.setIcon(R.drawable.sub8_spring1);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
                	Intent intent = new Intent(Sub8Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button1.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub8_spirng1=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
        
        final Button button2 = (Button)findViewById(R.id.button2);  //Sub3spring2Activity (백두대간 내면 나물축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub8_spirng2==0)
            button2.setBackgroundResource(R.drawable.where);
           else
        	   button2.setBackgroundResource(R.drawable.changewhere);
        button2.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=35.3754985;
      		     MapActivity.lng=126.543353;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub8Activity.this);
        		bld.setTitle("고창 청보리밭 축제 ");
        	   bld.setMessage("보리가 건강식품으로 자리매김 되면서 쌀보다도 더 귀하게 대접받는 시대가 되었습니다.이와 더불어 보리밭 경관도 상당히 대접을 받게 되었습니다.이에 보답코자 지역주민들과 힘을 합쳐 더 아름답고 더 풍성한 보리밭을 만들어 가고 있습니다. 축제의 프로그램과 지역특산물 준비에도 힘을 모으고 있습니다. 탁 트인 들판의 청보리밭 속에서 봄의 향기를 듬뿍 맛 보십시오. 장소전북 고창군 학원관광농원 일원기간2013.04.20~2013.05.12");
        		
        	 bld.setIcon(R.drawable.sub8_spring2);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub8Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button2.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub8_spirng2=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
        final Button button3 = (Button)findViewById(R.id.button3);  //Sub3spring3Activity (커피나무축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub8_spirng3==0)
            button3.setBackgroundResource(R.drawable.where);
           else
        	   button3.setBackgroundResource(R.drawable.changewhere);
        button3.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=35.8294004;
      		     MapActivity.lng=127.1343618;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub8Activity.this);
        		bld.setTitle(" 덕진구청 벚꽃축제");
        	   bld.setMessage("완주·전주 상생협력을 위한 자매결연지 농·특산물 직거래장터를 운영하여 상호 화합의 장을 마련함으로써 통합분위기 확산에 기여, 구청 앞 벚꽃 개화시기에 맞춰 통합사진전 등을 개최하여 시민들에게 완주와전주 통합에 대한 홍보 및 적극적 동참과 관심 유도 장소전북 전주시 대덕구청 일원기간2013.04.09~2013.04.10");
        		
        	 bld.setIcon(R.drawable.sub8_spring3);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub8Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button3.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub8_spirng3=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        final Button button4 = (Button)findViewById(R.id.button4);  //Sub3spring3Activity (커피나무축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub8_spirng4==0)
            button4.setBackgroundResource(R.drawable.where);
           else
        	   button4.setBackgroundResource(R.drawable.changewhere);
        button4.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {	
            	 MapActivity.lat=35.7316078;
      		     MapActivity.lng=127.0976336;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub8Activity.this);
        		bld.setTitle("모악산진달래화전축제");
        	   bld.setMessage("봄철 꽃이 흐드러지게 피는 4월. 모악산을 찾아 가족과 함께 화전을 드시면서 하루를 즐겨보시기 바랍니다. 국민토종축제인 모악산진달래화전축제는 그동안 전국제일의 민간주도 축제로 참가자 스스로가 만들어가는 축제입니다. 꽃피는 봄철. 어머니의 품이라는 모악산에 올라 하루를 즐겨보시기 바립니다 장소전북 완주군 모악산 대원사 일원기간2013.04.20~2013.04.20");
        		
        	 bld.setIcon(R.drawable.sub8_spring4);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub8Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
     	    	
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button4.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub8_spirng4=1;
      	         }
           });
        	 bld.show();
             }
        });
        
      
        
        /*여름축제에 관한 버튼* */	 
       final Button button5 = (Button)findViewById(R.id.button5);  //Sub3summer1Activity (의암제)정보로 이동하기 위한 버튼
        summercount++;
        if(flag.sub8_summer1==0)
            button5.setBackgroundResource(R.drawable.where);
           else
        	   button5.setBackgroundResource(R.drawable.changewhere);
        //button4.setPadding(10, 10, 10, 10);
        button5.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=36.010055;
      		     MapActivity.lng=127.7101386;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub8Activity.this);
        		bld.setTitle("무주 반딧불 축제");
        	   bld.setMessage("무주반딧불축제는 국내 최초의 생태환경축제로서 꿈과 희망이 있는 축제 입니다. 무주반딧불축제는 반딧불이를 통해 대자연과 하나되는 환경축제 빈딧불 빛으로 건강한 행복을 형상화한 감동축제 그리고 잊혀져가는 전통문화를 재현한 교육축제로 기억될 것 입니다. 장소전북 무주군 무주등나무운동장 등기간2013.06.01~2013.06.09");
        		
        	 bld.setIcon(R.drawable.sub8_summer1);
        	
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub8Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button5.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub8_summer1=1;
      	         }
           });
        	 bld.show();
             }
        });
        
       
        
       final Button button6 = (Button)findViewById(R.id.button6);  //Sub3summer2Activity (원주 장미축제)정보로 이동하기 위한 버튼
       // button5.setPadding(20, 20, 20, 20);
        summercount++;
        if(flag.sub8_summer2==0)
            button6.setBackgroundResource(R.drawable.where);
           else
        	   button6.setBackgroundResource(R.drawable.changewhere);
        button6.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=36.0045828;
      		     MapActivity.lng=127.3808254;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub8Activity.this);
        		bld.setTitle("진안 운일암 반일암 무릉계곡 산삼축제");
        	   bld.setMessage("맑고 고운 계곡과 원시림이 어울림하는 이 곳 무릉계곡에서 산양산삼과 야생약초를 자연과 함께 가꾸었습니다. 조금 더 자연에 가까이 가고 싶어하는 우리에게 자연과 함께 가꾸어 나갈 수 있는 희망의 비전과 우리의 향연을 펼치고져 합니다. 대자연의 푸르름과 함께 펼쳐지는 산삼축제와 숲속장터에 시민여러분을 초대합니다. 장소전북 진안군 무릉리 산 57번지기간2013.05.03~2013.06.02");
        		
        	 bld.setIcon(R.drawable.sub8_summer2);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub8Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button6.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub8_summer2=1;
      	         }
           });
        	 bld.show();
             }
        });
        
     
        final Button button7 = (Button)findViewById(R.id.button7);  //Sub3summer3Activity (강릉단오제)정보로 이동하기 위한 버튼
        summercount++;
        if(flag.sub8_summer3==0)
            button7.setBackgroundResource(R.drawable.where);
           else
        	   button7.setBackgroundResource(R.drawable.changewhere);
        button7.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=35.8477376;
      		     MapActivity.lng=127.121609;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub8Activity.this);
        		bld.setTitle("전주단오제");
        	   bld.setMessage("2013년 쉰다섯째를 맞는 전주단오는 단오의 세시풍속을 알리고 체험할 수 있는 전통문화의 교육의 장 마련과 더불어 전주단오만의 정체성을 보여드리는 축제입니다. 장소전북 전주시 덕진공원 일대기간2013.06.13~2013.06.14");
        		
        	 bld.setIcon(R.drawable.sub8_summer3);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub8Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button7.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub8_summer3=1;
      	         }
           });
        	 bld.show();
             }
        });
       
        /*가을축제에 관한 버튼* */
       final Button button8 = (Button)findViewById(R.id.button8);   //Sub3fall1Activity (정선아리랑제 축제)정보로 이동하기 위한 버튼
        fallcount++;
        if(flag.sub8_fall1==0)
            button8.setBackgroundResource(R.drawable.where);
           else
        	   button8.setBackgroundResource(R.drawable.changewhere);
        button8.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=36.7124951;
      		     MapActivity.lng=127.057747;

            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub8Activity.this);
        		bld.setTitle("전주세계소리축제");
        	   bld.setMessage("우리 전통음악인 판소리에 근간을 두고 세계음악과의 벽을 허무는 전주세계소리축제는 특정 음악 장르에 치우치지 않고 누구나 참여할 수 있는 프린지에서 부터 각 분야별로 세계적인 명성을 얻고 있는 마스터급 아티스트 공연까지 다양한 공연을 한자리에서 느낄 수 있는 고품격 세계음악예술제입니다. 장소전북 전주시 한국소리문화의전당 등기간2013.10.02~2013.10.06");
        		
        	 bld.setIcon(R.drawable.sub8_fall1);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub8Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button8.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub8_fall1=1;
      	         }
           });
        	 bld.show();
             }
        });
      
        
       final Button button9 = (Button)findViewById(R.id.button9);   //Sub3fall2Activity (양양송이축제)정보로 이동하기 위한 버튼
        fallcount++; 
        if(flag.sub8_fall2==0)
            button9.setBackgroundResource(R.drawable.where);
           else
        	   button9.setBackgroundResource(R.drawable.changewhere);
        button9.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=35.7944193;
      		     MapActivity.lng=126.8114269;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub8Activity.this);
        		bld.setTitle("김제 지평선 축제 ");
        	   bld.setMessage("올 가을 김제에 방문하셔서 김제쌀의 진미와 가을의 진한 정취를 느껴보셨으면 합니다. 황금들판에 어우러진 코스모스의 향연과 체험속에서 느껴지는 포근하고 편안한 고향의 정을 가득 담아 가시길 바랍니다. 장소전북 김제시 벽골제 일원기간2013.10.02~2013.10.06");
        		
        	 bld.setIcon(R.drawable.sub8_fall2);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub8Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button9.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub8_fall2=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
       
        /*봄, 여름, 가을, 겨울 구분하기 위한 탭 생성 * */
        TabHost tabHost = (TabHost)findViewById(R.id.tabHost);
        tabHost.setup();
     
        TabHost.TabSpec spec = tabHost.newTabSpec("tag1"); //봄을 나타내는 tab1
       spec.setContent(R.id.tab1);
       spec.setIndicator("", this.getResources().getDrawable(R.drawable.tab1));   	      	
       tabHost.addTab(spec);

        
        spec = tabHost.newTabSpec("tag2");  //여름을 나타내는 tab2
        spec.setContent(R.id.tab2);
        spec.setIndicator("", this.getResources().getDrawable(R.drawable.tab2));   	      	
        tabHost.addTab(spec);
        
        
        spec = tabHost.newTabSpec("tag3"); //가을을 나타내는 tab3
        spec.setContent(R.id.tab3);
        spec.setIndicator("", this.getResources().getDrawable(R.drawable.tab3));   	      	 
        tabHost.addTab(spec);
        
        spec = tabHost.newTabSpec("tag5"); //겨울을 나타내는 tab4
        spec.setContent(R.id.tab5);
        spec.setIndicator("", this.getResources().getDrawable(R.drawable.tab4));   	      	
        tabHost.addTab(spec);
        
     
        tabHost.setCurrentTab(0);        
         }
       }
}