/** 
 * Title: Festival navigation
* @autor : kyjhg1@naver.com
* Last Update : 2013/05/26
* version 4.0
*/ 
package com.example.ftnavi;
 
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Window;
 
/*activity_loading 레이아웃을 위한 클래스
        앱 처음시작 시 로딩화면을 보여준다
* */
public class SubActivity extends Activity{
	@Override
	public void onCreate(Bundle savedInstancestate){
		super.onCreate(savedInstancestate);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_loading);
		
		Handler handler = new Handler(){
			@Override
			public void handleMessage(Message msg){
				finish(); //3초후 꺼진다.
			}
		};
		
		handler.sendEmptyMessageDelayed(0, 3000); //3초동안 보여줌
	}
}
	