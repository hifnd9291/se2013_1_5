/** 
 * Title: Festival navigation
* @autor : kyjhg1@naver.com
* Last Update : 2013/05/26
* version 4.0
*/ 
package com.example.ftnavi;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;
import android.widget.TabHost;

/*activity_sub3을 위한 클래스
       강원도 레이아웃을 보여주기 위한 클래스
* */
public class Sub3Activity extends Activity 
{
	private int springcount=0; //강원도의 봄축제 count
	private int summercount=0; //강원도의 여름축제 count
	private int fallcount=0;   //강원도의 가을축제 count
    private int wintercount=0; //강원도의 겨울축제 count

    /** Called when the activity is first created. */
    @Override
    /*
    @param :activity_sub3을 위한 메소드
    * */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_sub3);
         {
        	 
        	  /*봄축제에 관한 버튼* */
        	 /*클릭하면 축제의 정보보여주기위한 대화상자가 나타난다* */
        final Button button1 = (Button)findViewById(R.id.button1);  //Sub3spring1Activity (양구곰취축제)정보로 이동하기 위한 버튼
        springcount++;
    {
    	//플래그가 0이라면 빨간버튼, 1이라면 파란색버튼으로 이미지를 지정한다.
       if(flag.sub3_spirng1==0)
        button1.setBackgroundResource(R.drawable.where);
       else
    	   button1.setBackgroundResource(R.drawable.changewhere);
        
        button1.setOnClickListener(new Button.OnClickListener()
        {
        	
             public void onClick(View v) 
             {		
            	 //해당축제에 위도와 경도를 지정한다.
            	 MapActivity.lat=38.209054;
          		MapActivity.lng=128.0607906;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub3Activity.this);
        		bld.setTitle("양구곰취축제   ");
        	   bld.setMessage("지역특산물인 곰취나물의 우수성을 널리 알려 수요를 창출하고 재배농가의 생산의욕고취와 군민화합의 장을 마련하기 위해 2004년부터 매년 5월에 축제를 개최 전국에 홍보함으로서 곰취와 곰취찐빵등 판매가 급증하고 있습니다. 특히 우편주문판매로 농가소득증대및 지역경제 활성화를 도모하는 축제입니다.  장소: 원 양구군 팔랑폭포 일원   기간: 2013.05.17~2013.05.19");
        		
        	 bld.setIcon(R.drawable.sub3_spring1);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
                	
                	//지도클래스로 이동
                	Intent intent = new Intent(Sub3Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button1.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub3_spirng1=1; //해당 플래그를 1로 바꾸워서 축제 버튼의 이미지를 변경하기위해 사용
      	         }
           });
        		 
        	 bld.show();
             }
        });
        
        
        
        final Button button2 = (Button)findViewById(R.id.button2);  //Sub3spring2Activity (백두대간 내면 나물축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub3_spirng2==0)
            button2.setBackgroundResource(R.drawable.where);
           else
        	   button2.setBackgroundResource(R.drawable.changewhere);
        button2.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {
            	 MapActivity.lat=37.7737917;
           		MapActivity.lng=128.3897499;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub3Activity.this);
        		bld.setTitle(" 백두대간 내면 나물축제 ");
        	   bld.setMessage("올해 나물축제는 5월 17~18일 이틀간 내면고원체육공원에서 열리며 곰취, 명이, 참나물, 더덕, 누리대 등의 나물을 저렴한 가격에 판매한다. 또한 산나물 요리 경연대회를 개최할 계획이며 축제장에서 산나물 및 서각전시 등의 전시행사도 병행한다.  장소: 강원 홍천군 내면고원체육공원  기간: 2013.05.17~2013.05.18");
        		
        	 bld.setIcon(R.drawable.sub3_spring2);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub3Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button2.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub3_spirng2=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
        final Button button3 = (Button)findViewById(R.id.button3);  //Sub3spring3Activity (커피나무축제)정보로 이동하기 위한 버튼
        springcount++;
        if(flag.sub3_spirng3==0)
            button3.setBackgroundResource(R.drawable.where);
           else
        	   button3.setBackgroundResource(R.drawable.changewhere);
        button3.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=37.6524194;
            		MapActivity.lng=128.78491;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub3Activity.this);
        		bld.setTitle(" 커피나무축제");
        	   bld.setMessage("2013년 5월, 커피꽃 향기 가득한 커피농장에서 [제4회 2013 커피나무축제]가 열립니다. 대한민국 커피나무가 자라는 곳! 강릉커피농장으로 여러분을 초대합니다. 장소: 강원 강릉시 커피농장  기간:2013.05.17~2013.05.26");
        		
        	 bld.setIcon(R.drawable.sub3_spring3);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub3Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button3.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub3_spirng3=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
        /*여름축제에 관한 버튼 * */	 
        final Button button4 = (Button)findViewById(R.id.button4);  //Sub3summer1Activity (의암제)정보로 이동하기 위한 버튼
        summercount++;
        if(flag.sub3_summer1==0)
            button4.setBackgroundResource(R.drawable.where);
           else
        	   button4.setBackgroundResource(R.drawable.changewhere);
        //button4.setPadding(10, 10, 10, 10);
        button4.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=36.4822069;
         		MapActivity.lng=127.3931184;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub3Activity.this);
        		bld.setTitle("의암제");
        	   bld.setMessage("춘천이 낳은 의암 유인석 선생의 살신성인 호국정신과 그 유훈을 되새기며 시민정신으로 승화시키기 위해 매년 개최하는 추모행사입니다 .  장소: 강원 춘천시 의암류인석선생유적지  기간: 2013.06.01~2013.06.01");
        		
        	 bld.setIcon(R.drawable.sub3_summer1);
        	
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub3Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button4.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub3_summer1=1;
      	         }
           });
        	 bld.show();
             }
        });
        
       
        
        final Button button5 = (Button)findViewById(R.id.button5);  //Sub3summer2Activity (원주 장미축제)정보로 이동하기 위한 버튼
       // button5.setPadding(20, 20, 20, 20);
        summercount++;
        if(flag.sub3_summer2==0)
            button5.setBackgroundResource(R.drawable.where);
           else
        	   button5.setBackgroundResource(R.drawable.changewhere);
        button5.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	 MapActivity.lat=37.3364115;
          		MapActivity.lng=127.9462639;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub3Activity.this);
        		bld.setTitle("원주장미축제");
        	   bld.setMessage("장소:강원 원주시 장미공원  기간: 2013.06.07~2013.06.09");
        		
        	 bld.setIcon(R.drawable.sub3_summer2);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub3Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button5.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub3_summer2=1;
      	         }
           });
        	 bld.show();
             }
        });
        
     
        final Button button6 = (Button)findViewById(R.id.button6);  //Sub3summer3Activity (강릉단오제)정보로 이동하기 위한 버튼
        summercount++;
        if(flag.sub3_summer3==0)
            button6.setBackgroundResource(R.drawable.where);
           else
        	   button6.setBackgroundResource(R.drawable.changewhere);
        button6.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {	
            	 MapActivity.lat=37.7488008;
           		MapActivity.lng=128.895575;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub3Activity.this);
        		bld.setTitle("강릉단오제");
        	   bld.setMessage("현재의 강릉단오제는 음력 4월 5일부터 5월 9일까지 단오를 전후한 1개월간 진행되는 마을공동축제이다. 강릉단오제의 요점은 대관령에서 산신제를 지내고, 지역의 수호신인 국사성황신을 모셔와 영동지역 무속에서 신앙하는 여러 신들을 차례로 모시는 의례를 치른 후 국사성황신을 다시 보내드리는 것이다. 그 과정에 여러 제의와 굿거리, 난장놀이, 민속놀이가 함께한다. 따라서 예로부터 내려온 지정문화재행사가 중요행사로 진행되고 그밖에 민속놀이, 경축행사, 체험학습행사가 함께 치러진다  장소: 강원 강릉시 남대천단오장  기간: 2013.06.09~2013.06.16");
        		
        	 bld.setIcon(R.drawable.sub3_summer3);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub3Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button6.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub3_summer3=1;
      	         }
           });
        	 bld.show();
             }
        });
       
        /*가을축제에 관한 버튼 * */
        final Button button7 = (Button)findViewById(R.id.button7);   //Sub3fall1Activity (정선아리랑제 축제)정보로 이동하기 위한 버튼
        fallcount++;
        if(flag.sub3_fall1==0)
            button7.setBackgroundResource(R.drawable.where);
           else
        	   button7.setBackgroundResource(R.drawable.changewhere);
        button7.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {	
            	 MapActivity.lat=37.3802498;
            		MapActivity.lng=128.6602297;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub3Activity.this);
        		bld.setTitle("정선아리랑축제");
        	   bld.setMessage("한국을 대표하는 민요, 아리랑. 그 중에서도 강원도 무형문화재 1호 정선아리랑은 가락이 느리고 구성져서 강원도 산간 지방의 정서를 잘 담고 있는 노래다. 정선아리랑은 우리나라 아리랑 가운데 가사 수가 가장 많고 전승 보전이 잘 되어 있기도 하다. 장소:강원 정선군 정선문화예술회관  기간: 2013.10.02~2013.10.05 영동고속도로 속사나들목-6번도로 속사강릉방면-59번도로 태백정선방면-42번도로-정선");
        		
        	 bld.setIcon(R.drawable.sub3_fall1);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub3Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button7.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub3_fall1=1;
      	         }
           });
        	 bld.show();
             }
        });
      
        
        final Button button8 = (Button)findViewById(R.id.button8);   //Sub3fall2Activity (양양송이축제)정보로 이동하기 위한 버튼
        fallcount++; 
        if(flag.sub3_fall2==0)
            button8.setBackgroundResource(R.drawable.where);
           else
        	   button8.setBackgroundResource(R.drawable.changewhere);
        button8.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {		
            	
            	 MapActivity.lat=38.068447;
            		MapActivity.lng=128.6200985;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub3Activity.this);
        		bld.setTitle("양양송이축제");
        	   bld.setMessage("온몸으로 느끼는 천년의솔향, 양양송이 축제로 초대합니다. 초롱초롱 별이 쏟아질 듯한 밤하늘과 솔잎 향 가득한 대자연의 숨소리가 있습니다. 수확을 기다리는 꽉찬 곡식들과 곱게 물든 산천, 인심 넉넉한 전원의 삶이 있습니다. 수십년 자란 소나무 밑엔 수줍은 듯 모습을 감춘 황금송이가 있습니다. 장소: 강원 양양군 양양남대천  기간:2013.10.02~2013.10.06");
        		
        	 bld.setIcon(R.drawable.sub3_fall2);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub3Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button8.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub3_fall2 =1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
        /*겨울축제에 관한 버튼 * */
        final Button button10 = (Button)findViewById(R.id.button10);   //Sub3winter1Activity (태백산눈꽃축제)정보로 이동하기 위한 버튼
        wintercount++;
        if(flag.sub3_winter1==0)
            button10.setBackgroundResource(R.drawable.where);
           else
        	   button10.setBackgroundResource(R.drawable.changewhere);
        button10.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {	
            	 MapActivity.lat=37.177151;
         		MapActivity.lng=128.8931099;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub3Activity.this);
        		bld.setTitle("태백산눈꽃축제");
        	   bld.setMessage("눈의 왕국, 설레임이 가득한 태백산 눈축제에 여러분을 초대합니다. 한강과 낙동강의 발원지가 있는 도시로 무구한 역사성과 고원지대의 아름다움을 간직하고 있는 태백에서 오랫동안 기억에 남을 아름다운 추억을 만들어 가시기 바랍니다  장소: 태백산 도릭공원  기간: 2013.01.25~2013.02.03");
        		
        	 bld.setIcon(R.drawable.sub3_winter1);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub3Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button10.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub3_winter1=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
        final Button button11 = (Button)findViewById(R.id.button11);  //Sub3winter2Activity (홍천강 꽁꽁축제 )정보로 이동하기 위한 버튼
        wintercount++;
        if(flag.sub3_winter2==0)
            button11.setBackgroundResource(R.drawable.where);
           else
        	   button11.setBackgroundResource(R.drawable.changewhere);
        button11.setOnClickListener(new Button.OnClickListener()
        {
             public void onClick(View v) 
             {	
            	 MapActivity.lat=37.6866391;
          		MapActivity.lng=127.8833287;
            	//버튼클릭시 진동을 울리게 한다.
         		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
             	vib.vibrate(80);
        	   AlertDialog.Builder bld = new AlertDialog.Builder(Sub3Activity.this);
        		bld.setTitle("홍천강 꽁꽁축제");
        	   bld.setMessage("홍천강 꽁꽁축제는 다양한 체험은 물론 홍보관, 홍천관 어류 생태전시관 등 볼거리와 놀거리가 강화하여 다양하게 개최됩니다 . 맑고 깨끗한 홍천강에서 펼쳐지는 겨울의 최대낭만 축제를 가족과 함게 즐거운 시간이 되시길 바랍니다  장소: 홍천강 홍천교~ 남산교  기간: 2013.01.04~2013.01.20");
        		
        	 bld.setIcon(R.drawable.sub3_winter2);
        	 bld.setPositiveButton("지도보기", new DialogInterface.OnClickListener() 
     		{
     	    	public void onClick(DialogInterface dialog, int which)
     	    	{
     	    		//버튼클릭시 진동을 울리게 한다.
            		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                	vib.vibrate(80);
     				//지도보기
                	Intent intent = new Intent(Sub3Activity.this, MapActivity.class);
            		startActivity(intent);
     	         }
          });
        	 bld.setNegativeButton("나의축제등록하기",new DialogInterface.OnClickListener() 
        	 {
        		 public void onClick(DialogInterface dialog, int which)
      	    	{
      	    		//버튼클릭시 진동을 울리게 한다.
             		Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                 	vib.vibrate(80);
                 	button11.setBackgroundResource(R.drawable.changewhere);
                 	flag.sub3_winter2=1;
      	         }
           });
        	 bld.show();
             }
        });
        
        
        /*봄, 여름, 가을, 겨울 구분하기 위한 탭 생성* */
        TabHost tabHost = (TabHost)findViewById(R.id.tabHost);
        tabHost.setup();
     
        TabHost.TabSpec spec = tabHost.newTabSpec("tag1"); //봄을 나타내는 tab1
       spec.setContent(R.id.tab1);
       spec.setIndicator("", getResources().getDrawable(R.drawable.tab1));   	      	
       tabHost.addTab(spec);

        
        spec = tabHost.newTabSpec("tag2");  //여름을 나타내는 tab2
        spec.setContent(R.id.tab2);
        spec.setIndicator("", getResources().getDrawable(R.drawable.tab2));   	      	
        tabHost.addTab(spec);
        
        
        spec = tabHost.newTabSpec("tag3"); //가을을 나타내는 tab3
        spec.setContent(R.id.tab3);
        spec.setIndicator("", getResources().getDrawable(R.drawable.tab3));   	      	 
        tabHost.addTab(spec);
        
        spec = tabHost.newTabSpec("tag5"); //겨울을 나타내는 tab4
        spec.setContent(R.id.tab5);
        spec.setIndicator("", getResources().getDrawable(R.drawable.tab4));   	      	
        tabHost.addTab(spec);
        
     
        tabHost.setCurrentTab(0);        
         }
       }
}
}
