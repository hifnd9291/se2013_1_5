/** 
 * Title: Festival navigation
* @autor : kyjhg1@naver.com
* Last Update : 2013/05/26
* version 4.0
*/ 
package com.example.ftnavi;

/*
축제 정보 대화상자안에서 나의축제등록하기 버튼을 클릭시 축제의 버튼이미지가 빨간색에서 파란색으로 변화하기 위한 플래그.
* */
class flag
{   //sub3의 축제들의 플래그들
	static int sub3_spirng1 =0;
	static int sub3_spirng2 =0;
	static int sub3_spirng3 =0;
	static int sub3_summer1 =0;
	static int sub3_summer2 =0;
	static int sub3_summer3 =0;
	static int sub3_fall1 =0;
	static int sub3_fall2 =0;
	static int sub3_winter1 =0;
	static int sub3_winter2 =0;
	
	//sub4의 축제들의 플래그들
	static int sub4_spirng1 =0;
	static int sub4_spirng2 =0;
	static int sub4_spirng3 =0;
	static int sub4_spirng4 =0;
	static int sub4_summer1 =0;
	static int sub4_summer2 =0;
	static int sub4_summer3 =0;
	static int sub4_fall1 =0;
	static int sub4_fall2 =0;
	static int sub4_fall3 =0;
	static int sub4_winter1 =0;
	
	//sub5의 축제들의 플래그들
	static int sub5_spirng1 =0;
	static int sub5_spirng2 =0;
	static int sub5_spirng3 =0;
	static int sub5_spirng4 =0;
	static int sub5_spirng5 =0;
	static int sub5_spirng6 =0;
	static int sub5_spirng7 =0;
	static int sub5_spirng8 =0;
	static int sub5_summer1 =0;
	static int sub5_summer2 =0;
	static int sub5_summer3 =0;
	static int sub5_fall1 =0;
	static int sub5_fall2 =0;
	static int sub5_fall3 =0;
	static int sub5_winter1 =0;
	static int sub5_winter2 =0;
		
	//sub6의 축제들의 플래그들
	static int sub6_spirng1 =0;
	static int sub6_spirng2 =0;
	static int sub6_spring3=0;
	static int sub6_summer1 =0;
	static int sub6_summer2 =0;
	static int sub6_summer3 =0;
	static int sub6_fall1 =0;
	static int sub6_fall2 =0;
	static int sub6_fall3 =0;
		
	//sub7의 축제들의 플래그들
	static int sub7_spirng1 =0;
	static int sub7_spirng2 =0;
	static int sub7_spirng3 =0;
	static int sub7_spirng4 =0;
	static int sub7_spirng5 =0;
	static int sub7_spirng6 =0;
	static int sub7_summer1 =0;
	static int sub7_summer2 =0;
	static int sub7_summer3 =0;
	static int sub7_summer4 =0;
	static int sub7_fall1 =0;
	static int sub7_fall2 =0;
	static int sub7_winter1 =0;
				
	//sub8의 축제들의 플래그들
	static int sub8_spirng1 =0;
	static int sub8_spirng2 =0;
	static int sub8_spirng3 =0;
	static int sub8_spirng4 =0;
	static int sub8_summer1 =0;
	static int sub8_summer2 =0;
	static int sub8_summer3 =0;
	static int sub8_fall1 =0;
	static int sub8_fall2 =0;

	//sub9의 축제들의 플래그들
	static int sub9_spirng1 =0;
	static int sub9_spirng2 =0;
	static int sub9_spirng3 =0;
	static int sub9_summer1 =0;
	static int sub9_summer2 =0;
	static int sub9_summer3 =0;
	static int sub9_fall1 =0;
	static int sub9_fall2 =0;
	static int sub9_fall3 = 0;
	static int sub9_winter1 =0;
	static int sub9_winter2 =0;	
		
	//sub9의 축제들의 플래그들
	static int sub10_spirng1 =0;
	static int sub10_spirng2 =0;
	static int sub10_spirng3 =0;
	static int sub10_summer1 =0;
	static int sub10_summer2 =0;
	static int sub10_summer3 =0;
	static int sub10_fall1 =0;
	static int sub10_fall2 =0;
	static int sub10_fall3 = 0;
}